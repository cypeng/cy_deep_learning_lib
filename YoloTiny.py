# -*- coding: utf-8 -*-
"""
https://github.com/dEcmir/lego_yolo/blob/master/train.py
"""
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import tensorflow as tf
import NetworkLib as NL
from tensorflow.contrib.layers import flatten

class YoloTiny():
    def __init__(self, learning_rate = 0.001, Mode = None, ParaList = None, ParaSetting = None):
        self.mode = Mode
        self.Yolo_setting = ParaSetting
        self.para_list = ParaList
        self.para_setting()
        self.model_setting()

        self.learning_rate = learning_rate
        self.tran_variable_list = None #NL.get_multi_training_variable_list(var_list=train_var_scope_list)
        self.loss_operation = None
        self.training_operation = None
        self.accuracy_operation = None
        #self.loss_operation = sess.graph.get_tensor_by_name("loss_operation/loss_operation:0")
        #self.accuracy_operation = sess.graph.get_tensor_by_name("accuracy_operation/accuracy_operation:0")

    def para_setting(self):
        if self.mode is None:
            self.__Yolo_init__()
        elif self.mode is "default_load":
            self.__Yolo_init__()
        #elif self.mode is "load":
        #    self.__translearning_LeNet5_init__()
        #elif self.mode is "translearn":
        #    self.__translearning_LeNet5_init__()

    def model_setting(self):
        # Raw Model Training
        if self.mode is None:
            self.supervisor_learning_input_default()
            self.y_pred1, self.y_pred2 = self.model_defualt_layerAPI()
            #self.y_pred1, self.y_pred2 = self.model_defualt()
        elif self.mode is "default_load":
            self.supervisor_learning_input_default()
            self.y_pred1, self.y_pred2 = self.model_defualt_layerAPI()
            #self.y_pred1, self.y_pred2 = self.model_defualt()
        #elif self.mode is "load":
        #    self.load_model()
        #elif self.mode is "translearn":
        #    self.supervisor_learning_input_default()
        #    self.y_pred = self.translearning_model()
        #self.one_hot_y = tf.one_hot(self.y, 10)

    def load_model(self, sess):
        #graph = sess.tf.get_default_graph()
        #print(tf.get_default_graph().as_graph_def())
        print(sess)
        self.x, self.y, self.y_pred = self.__load_model_io_init__(sess)

    def set_tran_variable_scope(self, var_list = None):
        self.tran_variable_list = NL.get_multi_training_variable_list(var_list=var_list)
        self.__estimation__()
        self.__backpropagation__()

    def __backpropagation__(self):
        with tf.name_scope("training_operation"):
            optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
            training_operation = optimizer.minimize(self.loss_operation,
                                                    var_list=self.tran_variable_list)
            tf.add_to_collection("training_operation", training_operation)

            self.training_operation = training_operation

    def __iou__(self, box_center, box_length, p_box_center, p_box_length):
        y_wr = box_center[:, :, 0] + box_length[:, :, 0] * 0.5
        y_wl = box_center[:, :, 0] - box_length[:, :, 0] * 0.5
        yp_wr = p_box_center[:, :, 0] + p_box_length[:, :, 0] * 0.5
        yp_wl = p_box_center[:, :, 0] - p_box_length[:, :, 0] * 0.5
        y_wr = tf.maximum(y_wr, yp_wr)
        y_wl = tf.minimum(y_wl, yp_wl)
        y_ht = box_center[:, :, 1] + box_length[:, :, 1] * 0.5
        y_hl = box_center[:, :, 1] - box_length[:, :, 1] * 0.5
        yp_ht = p_box_center[:, :, 1] + p_box_length[:, :, 1] * 0.5
        yp_hl = p_box_center[:, :, 1] - p_box_length[:, :, 1] * 0.5
        y_ht = tf.maximum(y_ht, yp_ht)
        y_hl = tf.minimum(y_hl, yp_hl)
        intersection1 = (y_wr - y_wl) * (y_ht - y_hl)
        union1 = box_length[:, :, 0] * box_length[:, :, 1] + \
                 p_box_length[:, :, 0] * p_box_length[:, :, 1] - \
                 intersection1
        return intersection1 / union1

    def __model_accuracy__(self):
        n_classes = 20
        out_height1, out_width1, out_channels1 = 416 // 32, 416 // 32, 3 * (5 + n_classes)
        out_height2, out_width2, out_channels2 = 2 * (416 // 32), 2 * (416 // 32), 3 * (5 + n_classes)
        true_y1 = tf.reshape(self.y1, [-1, 3 * out_height1 * out_width1, (5 + n_classes)])
        true_y2 = tf.reshape(self.y2, [-1, 3 * out_height2 * out_width2, (5 + n_classes)])
        y1_obj, y1_box_center, y1_box_length, y1_i_class = tf.split(true_y1, [1, 2, 2, n_classes], axis=-1)
        y1p_obj, y1p_box_center, y1p_box_length, y1p_i_class = tf.split(self.y_pred1, [1, 2, 2, n_classes], axis=-1)
        y2_obj, y2_box_center, y2_box_length, y2_i_class = tf.split(true_y2, [1, 2, 2, n_classes], axis=-1)
        y2p_obj, y2p_box_center, y2p_box_length, y2p_i_class = tf.split(self.y_pred2, [1, 2, 2, n_classes], axis=-1)

        accuracy1 = self.__iou__(y1_box_center, y1_box_length, y1p_box_center, y1p_box_length)
        accuracy_operation1 = tf.reduce_mean(accuracy1)
        accuracy2 = self.__iou__(y2_box_center, y2_box_length, y2p_box_center, y2p_box_length)
        accuracy_operation2 = tf.reduce_mean(accuracy2)

        correct_prediction3 = tf.equal(tf.argmax(y1p_i_class, 1), tf.argmax(y1_i_class, 1))
        correct_prediction4 = tf.equal(tf.argmax(y2p_i_class, 1), tf.argmax(y2_i_class, 1))
        accuracy_operation3 = tf.reduce_mean(tf.cast(correct_prediction3, tf.float32))
        accuracy_operation4 = tf.reduce_mean(tf.cast(correct_prediction4, tf.float32))
        tf.add_to_collection("accuracy_operation1", accuracy_operation1)
        tf.add_to_collection("accuracy_operation2", accuracy_operation2)
        tf.add_to_collection("accuracy_operation3", accuracy_operation3)
        tf.add_to_collection("accuracy_operation4", accuracy_operation4)
        return accuracy_operation1+accuracy_operation2+accuracy_operation3+accuracy_operation4

    def __model_loss__(self):
        batch_size, height, width, in_channels = self.x.get_shape().as_list()
        n_classes = 20
        out_height1, out_width1, out_channels1= 416 // 32, 416 // 32, 3 * (5 + n_classes)
        out_height2, out_width2, out_channels2 = 2*(416 // 32), 2*(416 // 32), 3 * (5 + n_classes)

        true_y1 = tf.reshape(self.y1, [-1, 3 * out_height1 * out_width1, (5 + n_classes)])
        y1_obj, y1_box_center, y1_box_length, y1_i_class = tf.split(true_y1, [1, 2, 2, n_classes], axis=-1)
        y1p_obj, y1p_box_center, y1p_box_length, y1p_i_class = tf.split(self.y_pred1, [1, 2, 2, n_classes], axis=-1)

        true_y2 = tf.reshape(self.y2, [-1, 3 * out_height2 * out_width2, (5 + n_classes)])
        y2_obj, y2_box_center, y2_box_length, y2_i_class = tf.split(true_y2, [1, 2, 2, n_classes], axis=-1)
        y2p_obj, y2p_box_center, y2p_box_length, y2p_i_class = tf.split(self.y_pred2, [1, 2, 2, n_classes], axis=-1)
        #print(y2_obj, y2_box_center, y2_box_length, y2_i_class)
        #print(y2p_obj, y2p_box_center, y2p_box_length, y2p_i_class)

        # Coordinate Loss
        weight_coord_loss = 1
        loss1_xy = weight_coord_loss * tf.reduce_mean(
            (y1p_box_center[:, :, 0] - y1_box_center[:, :, 0]) ** 2 +
            (y1p_box_center[:, :, 1] - y1_box_center[:, :, 1]) ** 2)
        loss2_xy = weight_coord_loss * tf.reduce_mean(
            (y2p_box_center[:, :, 0] - y2_box_center[:, :, 0]) ** 2 +
            (y2p_box_center[:, :, 1] - y2_box_center[:, :, 1]) ** 2)
        loss1_wh = weight_coord_loss * tf.reduce_mean(
            (y1p_box_length[:, :, 0] ** 0.5 - y1_box_length[:, :, 0] ** 0.5) ** 2 +
            (y1p_box_length[:, :, 1] ** 0.5 - y1_box_length[:, :, 1] ** 0.5) ** 2)
        loss2_wh = weight_coord_loss * tf.reduce_mean(
            (y2p_box_length[:, :, 0] ** 0.5 - y2_box_length[:, :, 0] ** 0.5) ** 2 +
            (y2p_box_length[:, :, 1] ** 0.5 - y2_box_length[:, :, 1] ** 0.5) ** 2)
        loss_xy = loss1_xy + loss2_xy
        loss_wh = loss1_wh + loss2_wh

        # IOU Loss
        weight_iou_loss = 1
        #print(tf.tile(y1_obj, (1, 1, n_classes)))
        loss1_obj = (-1) * tf.reduce_mean(
            tf.tile(y1_obj, (1, 1, n_classes)) * (
                    y1_i_class * tf.log(y1p_i_class) + (1 - y1_i_class) * tf.log(1 - y1p_i_class)))
        loss2_obj = (-1) * tf.reduce_mean(
            tf.tile(y2_obj, (1, 1, n_classes)) * (
                    y2_i_class * tf.log(y2p_i_class) + (1 - y2_i_class) * tf.log(1 - y2p_i_class)))
        loss1_noobj = weight_iou_loss * -1 * tf.reduce_mean(
            tf.tile(1 - y1_obj, (1, 1,  n_classes)) * (y1_i_class * tf.log(y1p_i_class) +
                                                   (1 - y1_i_class) * tf.log(1 - y1p_i_class)))
        loss2_noobj = weight_iou_loss * -1 * tf.reduce_mean(
            tf.tile(1 - y2_obj, (1, 1, n_classes)) * (y2_i_class * tf.log(y2p_i_class) +
                                                   (1 - y2_i_class) * tf.log(1 - y2p_i_class)))
        loss_obj = loss1_obj + loss2_obj
        loss_noobj = loss1_noobj + loss2_noobj

        # Confidence Loss
        loss1_p = (-1) * tf.reduce_mean(
            tf.tile(y1_obj, (1, 1,  n_classes)) * tf.log(tf.tile(y1p_obj, (1, 1,  n_classes)) * y1_i_class) +
            (1 - tf.tile(y1_obj, (1, 1,  n_classes)) * tf.log(1 - tf.tile(y1p_obj, (1, 1,  n_classes)) * y1_i_class)))
        loss2_p = (-1) * tf.reduce_mean(
            tf.tile(y2_obj, (1, 1,  n_classes)) * tf.log(tf.tile(y2p_obj, (1, 1,  n_classes)) * y2_i_class) +
            (1 - tf.tile(y2_obj, (1, 1,  n_classes)) * tf.log(1 - tf.tile(y2p_obj, (1, 1,  n_classes)) * y2_i_class)))
        loss_p = loss1_p + loss2_p

        return loss_xy + loss_wh, loss_obj + loss_noobj, loss_p

    def __estimation__(self):
        with tf.name_scope("loss_operation"):
            # Training Stage Operation
            lossC_operation, lossI_operation, lossP_operation = self.__model_loss__()
            #loss_operation = lossC_operation, lossI_operation, lossP_operation
            tf.add_to_collection("loss_operation_C", lossC_operation)
            tf.add_to_collection("loss_operation_I", lossI_operation)
            tf.add_to_collection("loss_operation_P", lossP_operation)
            self.lossC_operation, self.lossI_operation, self.lossP_operation = \
                lossC_operation, lossI_operation, lossP_operation
            self.loss_operation = lossC_operation+ lossI_operation+ lossP_operation

            with tf.name_scope("accuracy_operation"):
                self.accuracy_operation = self.__model_accuracy__()

    def __Yolo_init__(self):
        self.para_list = ["var_scope", "name_scope", "collection_name", "data_type", "kernel_shape", "bias_shape",
                          "stride_shape", "padding_type", "activation_func"]
        p = self.para_list
        Yolo_setting = {"layer_depth": 8,
                         "x": {p[0]: "x", p[1]: "input", p[2]: "IO", p[3]: tf.float32, p[4]: [None, 28, 28, 1],
                               p[5]: None, p[6]: None, p[7]: None, p[8]: None},
                         "y": {p[0]: "y", p[1]: "output", p[2]: "IO", p[3]: tf.int32, p[4]: [None],
                               p[5]: None, p[6]: None, p[7]: None, p[8]: None},
                         0: {p[0]: "conv1", p[1]: "conv1", p[2]: "conv1", p[3]: None, p[4]: [5, 5, 1, 6],
                             p[5]: [6], p[6]: [1, 1, 1, 1], p[7]: "SAME", p[8]: tf.nn.relu},
                         1: {p[0]: "pool1", p[1]: "pool1", p[2]: "pool1", p[3]: None, p[4]: [1, 2, 2, 1],
                             p[5]: None, p[6]: [1, 2, 2, 1], p[7]: "VALID", p[8]: None},
                         2: {p[0]: "conv2", p[1]: "conv2", p[2]: "conv2", p[3]: None, p[4]: [5, 5, 6, 16],
                             p[5]: [16], p[6]: [1, 1, 1, 1], p[7]: "VALID", p[8]: tf.nn.relu},
                         3: {p[0]: "pool2", p[1]: "pool2", p[2]: "pool2", p[3]: None, p[4]: [1, 2, 2, 1],
                             p[5]: None, p[6]: [1, 2, 2, 1], p[7]: "VALID", p[8]: None},
                         4: {p[0]: "flatten1", p[1]: "flatten", p[2]: "flatten", p[3]: None, p[4]: None,
                             p[5]: None, p[6]: None, p[7]: None, p[8]: None},
                         5: {p[0]: "dense1", p[1]: "dense1", p[2]: "dense1", p[3]: None, p[4]: [400, 120],
                             p[5]: [120], p[6]: None, p[7]: None, p[8]: tf.nn.relu},
                         6: {p[0]: "dense2", p[1]: "dense2", p[2]: "dense2", p[3]: None, p[4]: [120, 84],
                             p[5]: [84], p[6]: None, p[7]: None, p[8]: tf.nn.relu},
                         7: {p[0]: "dense3", p[1]: "dense3", p[2]: "dense3", p[3]: None, p[4]: [84, 10],
                             p[5]: [10], p[6]: None,p[7]: None,  p[8]: None}}
        self.Yolo_setting = Yolo_setting

    def __load_model_io_init__(self, sess):
        p = self.para_list
        x = sess.graph.get_tensor_by_name(self.Yolo_setting["x"][p[1]]+"/"+self.Yolo_setting["x"][p[0]]+":0")
        y = sess.graph.get_tensor_by_name(self.Yolo_setting["y"][p[1]]+"/"+self.Yolo_setting["y"][p[0]]+":0")
        #i = graph.get_tensor_by_name(self.LeNet_setting["i"][p[1]]+"/"+self.LeNet_setting["i"][p[0]]+":0")
        try:
            o = sess.graph.get_tensor_by_name(self.Yolo_setting["o1"][p[1]]+"/"+self.Yolo_setting["o1"][p[0]]+":0")
        except:
            o = sess.graph.get_tensor_by_name(self.Yolo_setting["o2"][p[1]] + "/" + self.Yolo_setting["o2"][p[0]] + ":0")

        return x, y, o

    def supervisor_learning_input_default(self):
        p = self.para_list
        classes = 20
        out_height = 416 // 32
        out_width = 416 // 32
        out_channels = 3 * (5 + classes)

        with tf.name_scope(self.Yolo_setting["x"][p[1]]):
            x = tf.placeholder(shape = (None, 416, 416, 3),
                               dtype = tf.float32,
                               name = "input")
            tf.add_to_collection("model"+self.Yolo_setting["x"][p[2]], x)
        with tf.name_scope(self.Yolo_setting["y"][p[1]]):
            y1 = tf.placeholder(shape=(None, out_height, out_width, out_channels),
                                dtype=tf.float32,
                                name="output_1")
            y2 = tf.placeholder(shape=(None, 2 * out_height, 2 * out_width, out_channels),
                                dtype=tf.float32,
                                name="output_2")
            tf.add_to_collection("y1", y1)
            tf.add_to_collection("y1", y2)
        self.x, self.y1, self.y2 = x, y1, y2

    """
        layer     filters    size              input                output
    0 conv     16  3 x 3 / 1   416 x 416 x   3   ->   416 x 416 x  16  0.150 BFLOPs
    1 max          2 x 2 / 2   416 x 416 x  16   ->   208 x 208 x  16
    2 conv     32  3 x 3 / 1   208 x 208 x  16   ->   208 x 208 x  32  0.399 BFLOPs
    3 max          2 x 2 / 2   208 x 208 x  32   ->   104 x 104 x  32
    4 conv     64  3 x 3 / 1   104 x 104 x  32   ->   104 x 104 x  64  0.399 BFLOPs
    5 max          2 x 2 / 2   104 x 104 x  64   ->    52 x  52 x  64
    6 conv    128  3 x 3 / 1    52 x  52 x  64   ->    52 x  52 x 128  0.399 BFLOPs
    7 max          2 x 2 / 2    52 x  52 x 128   ->    26 x  26 x 128
    8 conv    256  3 x 3 / 1    26 x  26 x 128   ->    26 x  26 x 256  0.399 BFLOPs
    9 max          2 x 2 / 2    26 x  26 x 256   ->    13 x  13 x 256
   10 conv    512  3 x 3 / 1    13 x  13 x 256   ->    13 x  13 x 512  0.399 BFLOPs
   11 max          2 x 2 / 1    13 x  13 x 512   ->    13 x  13 x 512
   12 conv   1024  3 x 3 / 1    13 x  13 x 512   ->    13 x  13 x1024  1.595 BFLOPs
   13 conv    256  1 x 1 / 1    13 x  13 x1024   ->    13 x  13 x 256  0.089 BFLOPs
   14 conv    512  3 x 3 / 1    13 x  13 x 256   ->    13 x  13 x 512  0.399 BFLOPs
   15 conv    255  1 x 1 / 1    13 x  13 x 512   ->    13 x  13 x class  0.044 BFLOPs
   16 yolo
   17 route  13
   18 conv    128  1 x 1 / 1    13 x  13 x 256   ->    13 x  13 x 128  0.011 BFLOPs
   19 upsample            2x    13 x  13 x 128   ->    26 x  26 x 128
   20 route  19 8
   21 conv    256  3 x 3 / 1    26 x  26 x 384   ->    26 x  26 x 256  1.196 BFLOPs
   22 conv    255  1 x 1 / 1    26 x  26 x 256   ->    26 x  26 x class  0.088 BFLOPs
   23 yolo
    """
    def model_defualt_layerAPI(self):
        anchor1 = ((344, 319), (135, 169), (81, 82))
        anchor2 = ((37, 58), (23, 27), (10, 14))

        layer = tf.layers.conv2d(inputs=self.x, filters=16, kernel_size=(3, 3), strides=(1, 1),
                                 padding='SAME', use_bias=False, activation=tf.nn.leaky_relu,
                                 data_format='channels_last',trainable = True)
        layer = tf.layers.max_pooling2d(layer, (2, 2), (2, 2), padding='SAME', data_format='channels_last')

        layer = tf.layers.conv2d(inputs=layer, filters=32, kernel_size=(3, 3), strides=(1, 1),
                                 padding='SAME', use_bias=False, activation=tf.nn.leaky_relu,
                                 data_format='channels_last',trainable = True)
        layer = tf.layers.max_pooling2d(layer,  (2, 2), (2, 2), padding='SAME', data_format='channels_last')

        layer = tf.layers.conv2d(inputs=layer, filters=64, kernel_size=(3, 3), strides=(1, 1),
                                 padding='SAME', use_bias=False, activation=tf.nn.leaky_relu,
                                 data_format='channels_last',trainable = True)
        layer = tf.layers.max_pooling2d(layer,  (2, 2), (2, 2), padding='SAME', data_format='channels_last')

        layer = tf.layers.conv2d(inputs=layer, filters=128, kernel_size=(3, 3), strides=(1, 1),
                                 padding='SAME', use_bias=False, activation=tf.nn.leaky_relu,
                                 data_format='channels_last',trainable = True)
        layer = tf.layers.max_pooling2d(layer,  (2, 2), (2, 2), padding='SAME', data_format='channels_last')

        layer = tf.layers.conv2d(inputs=layer, filters=256, kernel_size=(3, 3), strides=(1, 1),
                                 padding='SAME', use_bias=False, activation=tf.nn.leaky_relu,
                                 data_format='channels_last',trainable = True)
        with tf.control_dependencies([layer]):
            route1 = tf.identity(layer)
        layer = tf.layers.max_pooling2d(layer,  (2, 2), (2, 2), padding='SAME', data_format='channels_last')

        layer = tf.layers.conv2d(inputs=layer, filters=512, kernel_size=(3, 3), strides=(1, 1),
                                 padding='SAME', use_bias=False, activation=tf.nn.leaky_relu,
                                 data_format='channels_last',trainable = True)
        layer = tf.layers.max_pooling2d(layer,  (2, 2), (1, 1), padding='SAME', data_format='channels_last')

        layer = tf.layers.conv2d(inputs=layer, filters=1024, kernel_size=(3, 3), strides=(1, 1),
                                 padding='SAME', use_bias=False, activation=tf.nn.leaky_relu,
                                 data_format='channels_last',trainable = True)

        layer = tf.layers.conv2d(inputs=layer, filters=256, kernel_size=(1, 1), strides=(1, 1),
                                 padding='SAME', use_bias=False, activation=tf.nn.leaky_relu,
                                 data_format='channels_last',trainable = True)
        with tf.control_dependencies([layer]):
            route2 = tf.identity(layer)
        layer = tf.layers.conv2d(inputs=layer, filters=512, kernel_size=(3, 3), strides=(1, 1),
                                 padding='SAME', use_bias=False, activation=tf.nn.leaky_relu,
                                 data_format='channels_last',trainable = True)
        layer = tf.layers.conv2d(inputs=layer, filters=75, kernel_size=(1, 1), strides=(1, 1),
                                 padding='SAME', use_bias=False, activation=None,
                                 data_format='channels_last',trainable = True)
        output1 = NL.yolo_detect(layer, anchor1, layer_name="YOLO1", collection_name="YOLO1")

        layer = tf.layers.conv2d(inputs=route2, filters=128, kernel_size=(1, 1), strides=(1, 1),
                                 padding='SAME', use_bias=False, activation=tf.nn.leaky_relu,
                                 data_format='channels_last',trainable = True)
        layer = NL.upsample(layer, layer.get_shape().as_list(), (1, 2, 2, 1), layer_name="upsample1", collection_name="upsample1")

        layer = tf.concat([layer, route1], 3)

        layer = tf.layers.conv2d(inputs=layer, filters=256, kernel_size=(3, 3), strides=(1, 1),
                                 padding='SAME', use_bias=False, activation=tf.nn.leaky_relu,
                                 data_format='channels_last',trainable = True)
        layer = tf.layers.conv2d(inputs=layer, filters=75, kernel_size=(1, 1), strides=(1, 1),
                                 padding='SAME', use_bias=False, activation=None,
                                 data_format='channels_last',trainable = True)
        #print(layer)
        output2 = NL.yolo_detect(layer, anchor2, layer_name="YOLO2", collection_name="YOLO2")
        #print(output2)
        return output1, output2

    def model_defualt(self):
        anchor1 = ((344, 319), (135, 169), (81, 82))
        anchor2 = ((37, 58), (23, 27), (10, 14))

        # 0 conv_0 = conv(0, "YOLO/input:0", 16, 3, 1)
        layer = NL.conv_layer(self.x, [3, 3, 3, 16], [16], [1, 1, 1, 1], "SAME", input_dim="2d",
                              gamma_shape = [1, 1, 1, 16], beta_shape = [1, 1, 1, 16],
                              weights_setting=None, batch_norm_flag=bool(1), activation_func=tf.nn.leaky_relu,
                              activation_para=0.1, var_scope="conv1", layer_name="conv1", collection_name="conv1")

        # 1 maxpool(1, "YOLO/conv_0/out:0", 2, 2)
        layer = NL.pooling_layer(layer, [1, 2, 2, 1], [1, 2, 2, 1], "SAME", input_dim="2d", pooling_type="MAX",
                                 layer_name="pool1", collection_name="pool1")

        # 2 conv(2, "YOLO/maxpool_1/out:0", 32, 3, 1)
        layer = NL.conv_layer(layer, [3, 3, 16, 32], [32], [1, 1, 1, 1], "SAME", input_dim="2d",
                              gamma_shape=[1, 1, 1, 32], beta_shape=[1, 1, 1, 32],
                              weights_setting=None, batch_norm_flag=bool(1), activation_func=tf.nn.leaky_relu,
                              activation_para=0.1, var_scope="conv2", layer_name="conv2", collection_name="conv2")

        # 3 maxpool(3, "YOLO/conv_2/out:0", 2, 2)
        layer = NL.pooling_layer(layer, [1, 2, 2, 1], [1, 2, 2, 1], "SAME", input_dim="2d", pooling_type="MAX",
                                 layer_name="pool2", collection_name="pool2")

        # 4 conv(4, "YOLO/maxpool_3/out:0", 64, 3, 1)
        layer = NL.conv_layer(layer, [3, 3, 32, 64], [64], [1, 1, 1, 1], "SAME", input_dim="2d",
                              gamma_shape=[1, 1, 1, 64], beta_shape=[1, 1, 1, 64],
                              weights_setting=None, batch_norm_flag=bool(1), activation_func=tf.nn.leaky_relu,
                              activation_para=0.1, var_scope="conv3", layer_name="conv3", collection_name="conv3")

        # 5 maxpool(5, "YOLO/conv_4/out:0", 2, 2)
        layer = NL.pooling_layer(layer, [1, 2, 2, 1], [1, 2, 2, 1], "SAME", input_dim="2d", pooling_type="MAX",
                                 layer_name="pool3", collection_name="pool3")

        # 6 conv(6, "YOLO/maxpool_5/out:0", 128, 3, 1)
        layer = NL.conv_layer(layer, [3, 3, 64, 128], [128], [1, 1, 1, 1], "SAME", input_dim="2d",
                              gamma_shape=[1, 1, 1, 128], beta_shape=[1, 1, 1, 128],
                              weights_setting=None, batch_norm_flag=bool(1), activation_func=tf.nn.leaky_relu,
                              activation_para=0.1, var_scope="conv4", layer_name="conv4", collection_name="conv4")

        # 7 maxpool(7, "YOLO/conv_6/out:0", 2, 2)
        layer = NL.pooling_layer(layer, [1, 2, 2, 1], [1, 2, 2, 1], "SAME", input_dim="2d", pooling_type="MAX",
                                 layer_name="pool4", collection_name="pool4")

        # 8 conv(8, "YOLO/maxpool_7/out:0", 256, 3, 1)
        layer = NL.conv_layer(layer, [3, 3, 128, 256], [256], [1, 1, 1, 1], "SAME", input_dim="2d",
                              gamma_shape=[1, 1, 1, 256], beta_shape=[1, 1, 1, 256],
                              weights_setting=None, batch_norm_flag=bool(1), activation_func=tf.nn.leaky_relu,
                              activation_para=0.1, var_scope="conv5", layer_name="conv5", collection_name="conv5")
        with tf.control_dependencies([layer]):
            route1 = tf.identity(layer)

        # 9  maxpool(9, "YOLO/conv_8/out:0", 2, 2)
        layer = NL.pooling_layer(layer, [1, 2, 2, 1], [1, 2, 2, 1], "SAME", input_dim="2d", pooling_type="MAX",
                                 layer_name="pool5", collection_name="pool5")

        # 10 conv(10, "YOLO/maxpool_9/out:0", 512, 3, 1)
        layer = NL.conv_layer(layer, [3, 3, 256, 512], [512], [1, 1, 1, 1], "SAME", input_dim="2d",
                              gamma_shape=[1, 1, 1, 512], beta_shape=[1, 1, 1, 512],
                              weights_setting=None, batch_norm_flag=bool(1), activation_func=tf.nn.leaky_relu,
                              activation_para=0.1, var_scope="conv6", layer_name="conv6", collection_name="conv6")

        # 11 maxpool(11, "YOLO/conv_10/out:0", 2, 1)
        layer = NL.pooling_layer(layer, [1, 2, 2, 1], [1, 1, 1, 1], "SAME", input_dim="2d", pooling_type="MAX",
                                 layer_name="pool6", collection_name="pool6")

        # 12 conv(12, "YOLO/maxpool_11/out:0", 1024, 3, 1)
        layer = NL.conv_layer(layer, [3, 3, 512, 1024], [1024], [1, 1, 1, 1], "SAME", input_dim="2d",
                              gamma_shape=[1, 1, 1, 1024], beta_shape=[1, 1, 1, 1024],
                              weights_setting=None, batch_norm_flag=bool(1), activation_func=tf.nn.leaky_relu,
                              activation_para=0.1, var_scope="conv7", layer_name="conv7", collection_name="conv7")

        # 13 conv(13, "YOLO/conv_12/out:0", 256, 1, 1)
        layer = NL.conv_layer(layer, [1, 1, 1024, 256], [256], [1, 1, 1, 1], "SAME", input_dim="2d",
                              gamma_shape=[1, 1, 1, 256], beta_shape=[1, 1, 1, 256],
                              weights_setting=None, batch_norm_flag=bool(1), activation_func=tf.nn.leaky_relu,
                              activation_para=0.1, var_scope="conv8", layer_name="conv8", collection_name="conv8")
        # 17 route(17, "YOLO/conv_13/out:0", None)
        with tf.control_dependencies([layer]):
            route2 = tf.identity(layer)

        # 14 conv(14, "YOLO/conv_13/out:0", 512, 3, 1, keep_prob=0.5)
        layer = NL.conv_layer(layer, [3, 3, 256, 512], [512], [1, 1, 1, 1], "SAME", input_dim="2d",
                              gamma_shape=[1, 1, 1, 512], beta_shape=[1, 1, 1, 512],
                              weights_setting=None, batch_norm_flag=bool(1), activation_func=tf.nn.leaky_relu,
                              activation_para=0.1, var_scope="conv9", layer_name="conv9", collection_name="conv9")

        # 15 conv(15, "YOLO/conv_14/out:0", 255, 1, 1, nonlin="linear", batchnorm=0)
        layer = NL.conv_layer(layer, [1, 1, 512, 75], [75], [1, 1, 1, 1], "SAME", input_dim="2d",
                              gamma_shape=[1, 1, 1, 75], beta_shape=[1, 1, 1, 75],
                              weights_setting=None, batch_norm_flag=bool(0), activation_func=None,
                              activation_para=None, var_scope="conv10", layer_name="conv10", collection_name="conv10")

        # 16 yolo(16, "YOLO/conv_15/out:0", anchor1)
        output1 = NL.yolo_detect(layer, anchor1, layer_name="YOLO1", collection_name="YOLO1")

        # 18 conv(18, "YOLO/route_17/out:0", 128, 1, 1, keep_prob=0.5)
        layer = NL.conv_layer(route2, [1, 1, 256, 128], [128], [1, 1, 1, 1], "SAME", input_dim="2d",
                              gamma_shape=[1, 1, 1, 128], beta_shape=[1, 1, 1, 128],
                              weights_setting=None, batch_norm_flag=bool(1), activation_func=tf.nn.leaky_relu,
                              activation_para=0.1, var_scope="conv11", layer_name="conv11", collection_name="conv11")

        # 19 upsample(19, "YOLO/conv_18/out:0", 2)
        layer = NL.upsample(layer, layer.get_shape().as_list(), layer_name="upsample1", collection_name="upsample1")

        # 20 route(20, "YOLO/upsample_19/out:0", "YOLO/conv_8/out:0")
        layer = tf.concat([layer, route1], 3)

        # 21 conv(21, "YOLO/route_20/out:0", 256, 3, 1)
        layer = NL.conv_layer(layer, [3, 3, 128, 256], [256], [1, 1, 1, 1], "SAME", input_dim="2d",
                              gamma_shape=[1, 1, 1, 256], beta_shape=[1, 1, 1, 256],
                              weights_setting=None, batch_norm_flag=bool(1), activation_func=tf.nn.leaky_relu,
                              activation_para=0.1, var_scope="conv12", layer_name="conv12", collection_name="conv12")

        # 22 conv(22, "YOLO/conv_21/out:0", 255, 1, 1, nonlin="linear", batchnorm=0)
        layer = NL.conv_layer(layer, [1, 1, 256, 75], [75], [1, 1, 1, 1], "SAME", input_dim="2d",
                              gamma_shape=[1, 1, 1, 75], beta_shape=[1, 1, 1, 75],
                              weights_setting=None, batch_norm_flag=bool(0), activation_func=None,
                              activation_para=None, var_scope="conv13", layer_name="conv13", collection_name="conv13")

        # 23 yolo(23, "YOLO/conv_22/out:0", anchor2)
        output2 = NL.yolo_detect(layer, anchor2, layer_name="YOLO2", collection_name="YOLO2")
        #output = tf.concat([output1, output2], axis=1)

        #print(output1, output2)

        return output1, output2
