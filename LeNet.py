# -*- coding: utf-8 -*-
"""
@author: CY Peng

DataVer  - Author - Note
2019/05/07   CY     Model Set
2019/08/13   C.Y.   TensorFlow Module Solution
2019/10/30   C.Y.   Model Type Choice
"""
import tensorflow as tf
import NetworkLib as NL
from tensorflow.contrib.layers import flatten

class LeNet():
    def __init__(self, learning_rate = 0.001, Mode = None, ParaList = None, ParaSetting = None):
        """
        :param learning_rate: for training
        :param Mode:
               default_load: Using tensorflow unit module to established the model
               default_unit_model_load: Using tensorflow layer module to established the model
               load: loading from the default_unit_model_load
               translearn: trasnfer learning from the default_unit_model_load
        :param ParaList: Training parameters list set
        :param ParaSetting: Training parameters set
        """
        self.mode = Mode
        self.LeNet_setting = ParaSetting
        self.para_list = ParaList
        self.para_setting()
        self.model_setting()

        self.learning_rate = learning_rate
        self.tran_variable_list = None #NL.get_multi_training_variable_list(var_list=train_var_scope_list)
        self.loss_operation = None
        self.training_operation = None
        self.accuracy_operation = None
        #self.model_type = 1 # 1: tensorflow module model 2: tensorflow unit model
        #self.loss_operation = sess.graph.get_tensor_by_name("loss_operation/loss_operation:0")
        #self.accuracy_operation = sess.graph.get_tensor_by_name("accuracy_operation/accuracy_operation:0")

    def para_setting(self):
        if self.mode is None:
            self.__LeNet5_init__()
        elif self.mode is "default_load" or self.mode is "default_unit_model_load":
            self.__LeNet5_init__()
        elif self.mode is "load":
            self.__translearning_LeNet5_init__()
        elif self.mode is "translearn":
            self.__translearning_LeNet5_init__()

    def model_setting(self):
        # Raw Model Training
        if self.mode is None:
            self.supervisor_learning_input_default()
            self.y_pred = self.tf_module_model_default()
            self.one_hot_y = tf.one_hot(self.y, 10)
        elif self.mode is "default_load" or self.mode is "default_unit_model_load":
            self.supervisor_learning_input_default()
            if self.mode is "default_load":
                self.y_pred = self.tf_module_model_default()
            elif self.mode is "default_unit_model_load":
                self.y_pred = self.model_defualt()
            self.one_hot_y = tf.one_hot(self.y, 10)
        elif self.mode is "load":
            self.load_model()
            self.one_hot_y = tf.one_hot(self.y, 10)
        elif self.mode is "translearn":
            self.supervisor_learning_input_default()
            self.y_pred = self.translearning_model()
            self.one_hot_y = tf.one_hot(tf.cast(self.y>=5, tf.int64), 2)

    def load_model(self, sess):
        #graph = sess.tf.get_default_graph()
        #print(tf.get_default_graph().as_graph_def())
        #print(sess)
        self.x, self.y, self.y_pred = self.__load_model_io_init__(sess)

    def set_tran_variable_scope(self, var_list = None):
        self.tran_variable_list = NL.get_multi_training_variable_list(var_list=var_list)
        self.__estimation__()
        self.__backpropagation__()

    def __backpropagation__(self):
        with tf.name_scope("training_operation"):
            optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
            training_operation = optimizer.minimize(self.loss_operation,
                                                    var_list=self.tran_variable_list)
            tf.add_to_collection("training_operation", training_operation)

            self.training_operation = training_operation

    def __estimation__(self):
        with tf.name_scope("loss_operation"):
            # Training Stage Operation
            cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits=self.y_pred, labels=self.one_hot_y)
            loss_operation = tf.reduce_mean(cross_entropy)
            tf.add_to_collection("loss_operation", loss_operation)
            self.loss_operation = loss_operation

        with tf.name_scope("accuracy_operation"):
            # Accuracy Calculation Operation
            correct_prediction = tf.equal(tf.argmax(self.y_pred, 1), tf.argmax(self.one_hot_y, 1))
            accuracy_operation = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
            tf.add_to_collection("accuracy_operation", accuracy_operation)
            self.accuracy_operation = accuracy_operation

    def __translearning_LeNet5_init__(self):
        self.para_list = ["var_scope", "name_scope", "collection_name", "data_type", "kernel_shape", "bias_shape",
                          "stride_shape", "padding_type", "activation_func"]
        p = self.para_list
        LeNet_setting = {"layer_depth": 1,
                         "x": {p[0]: "x", p[1]: "input", p[2]: "IO", p[3]: tf.float32, p[4]: [None, 28, 28, 1],
                               p[5]: None, p[6]: None, p[7]: None, p[8]: None},
                         "y": {p[0]: "y", p[1]: "output", p[2]: "IO", p[3]: tf.int32, p[4]: [None],
                               p[5]: None, p[6]: None, p[7]: None, p[8]: None},
                         #"i": {p[0]: "output", p[1]: "dense2", p[2]: None, p[3]: None, p[4]: [None],
                         #      p[5]: None, p[6]: None, p[7]: None, p[8]: None},
                         "o1": {p[0]: "output", p[1]: "dense3", p[2]: None, p[3]: None, p[4]: [None],
                               p[5]: None, p[6]: None, p[7]: None, p[8]: None},
                         "o2": {p[0]: "output", p[1]: "new_dense3", p[2]: None, p[3]: None, p[4]: [None],
                                p[5]: None, p[6]: None, p[7]: None, p[8]: None},
                         0: {p[0]: "conv1", p[1]: "conv1", p[2]: "conv1", p[3]: None, p[4]: [5, 5, 1, 6],
                             p[5]: [6], p[6]: [1, 1, 1, 1], p[7]: "SAME", p[8]: tf.nn.relu},
                         1: {p[0]: "pool1", p[1]: "pool1", p[2]: "pool1", p[3]: None, p[4]: [1, 2, 2, 1],
                             p[5]: None, p[6]: [1, 2, 2, 1], p[7]: "VALID", p[8]: None},
                         2: {p[0]: "conv2", p[1]: "conv2", p[2]: "conv2", p[3]: None, p[4]: [5, 5, 6, 16],
                             p[5]: [16], p[6]: [1, 1, 1, 1], p[7]: "VALID", p[8]: tf.nn.relu},
                         3: {p[0]: "pool2", p[1]: "pool2", p[2]: "pool2", p[3]: None, p[4]: [1, 2, 2, 1],
                             p[5]: None, p[6]: [1, 2, 2, 1], p[7]: "VALID", p[8]: None},
                         4: {p[0]: "flatten1", p[1]: "flatten", p[2]: "flatten", p[3]: None, p[4]: None,
                             p[5]: None, p[6]: None, p[7]: None, p[8]: None},
                         5: {p[0]: "dense1", p[1]: "dense1", p[2]: "dense1", p[3]: None, p[4]: [400, 120],
                             p[5]: [120], p[6]: None, p[7]: None, p[8]: tf.nn.relu},
                         6: {p[0]: "dense2", p[1]: "dense2", p[2]: "dense2", p[3]: None, p[4]: [120, 84],
                             p[5]: [84], p[6]: None, p[7]: None, p[8]: tf.nn.relu},
                         7: {p[0]: "new_dense3", p[1]: "new_dense3", p[2]: "new_dense3", p[3]: None, p[4]: [84, 2],
                             p[5]: [2], p[6]: None, p[7]: None, p[8]: None}}
        self.LeNet_setting = LeNet_setting


    def __LeNet5_init__(self):
        self.para_list = ["var_scope", "name_scope", "collection_name", "data_type", "kernel_shape", "bias_shape",
                          "stride_shape", "padding_type", "activation_func"]
        p = self.para_list
        LeNet_setting = {"layer_depth": 8,
                         "x": {p[0]: "x", p[1]: "input", p[2]: "IO", p[3]: tf.float32, p[4]: [None, 28, 28, 1],
                               p[5]: None, p[6]: None, p[7]: None, p[8]: None},
                         "y": {p[0]: "y", p[1]: "output", p[2]: "IO", p[3]: tf.int32, p[4]: [None],
                               p[5]: None, p[6]: None, p[7]: None, p[8]: None},
                         0: {p[0]: "conv1", p[1]: "conv1", p[2]: "conv1", p[3]: None, p[4]: [5, 5, 1, 6],
                             p[5]: [6], p[6]: [1, 1, 1, 1], p[7]: "SAME", p[8]: tf.nn.relu},
                         1: {p[0]: "pool1", p[1]: "pool1", p[2]: "pool1", p[3]: None, p[4]: [1, 2, 2, 1],
                             p[5]: None, p[6]: [1, 2, 2, 1], p[7]: "VALID", p[8]: None},
                         2: {p[0]: "conv2", p[1]: "conv2", p[2]: "conv2", p[3]: None, p[4]: [5, 5, 6, 16],
                             p[5]: [16], p[6]: [1, 1, 1, 1], p[7]: "VALID", p[8]: tf.nn.relu},
                         3: {p[0]: "pool2", p[1]: "pool2", p[2]: "pool2", p[3]: None, p[4]: [1, 2, 2, 1],
                             p[5]: None, p[6]: [1, 2, 2, 1], p[7]: "VALID", p[8]: None},
                         4: {p[0]: "flatten1", p[1]: "flatten", p[2]: "flatten", p[3]: None, p[4]: None,
                             p[5]: None, p[6]: None, p[7]: None, p[8]: None},
                         5: {p[0]: "dense1", p[1]: "dense1", p[2]: "dense1", p[3]: None, p[4]: [400, 120],
                             p[5]: [120], p[6]: None, p[7]: None, p[8]: tf.nn.relu},
                         6: {p[0]: "dense2", p[1]: "dense2", p[2]: "dense2", p[3]: None, p[4]: [120, 84],
                             p[5]: [84], p[6]: None, p[7]: None, p[8]: tf.nn.relu},
                         7: {p[0]: "dense3", p[1]: "dense3", p[2]: "dense3", p[3]: None, p[4]: [84, 10],
                             p[5]: [10], p[6]: None,p[7]: None,  p[8]: None}}
        self.LeNet_setting = LeNet_setting

    def __load_model_io_init__(self, sess):
        p = self.para_list
        x = sess.graph.get_tensor_by_name(self.LeNet_setting["x"][p[1]]+"/"+self.LeNet_setting["x"][p[0]]+":0")
        y = sess.graph.get_tensor_by_name(self.LeNet_setting["y"][p[1]]+"/"+self.LeNet_setting["y"][p[0]]+":0")
        #i = graph.get_tensor_by_name(self.LeNet_setting["i"][p[1]]+"/"+self.LeNet_setting["i"][p[0]]+":0")
        try:
            o = sess.graph.get_tensor_by_name(self.LeNet_setting["o1"][p[1]]+"/"+self.LeNet_setting["o1"][p[0]]+":0")
        except:
            o = sess.graph.get_tensor_by_name(self.LeNet_setting["o2"][p[1]] + "/" + self.LeNet_setting["o2"][p[0]] + ":0")

        return x, y, o

    def supervisor_learning_input_default(self):
        p = self.para_list
        with tf.name_scope(self.LeNet_setting["x"][p[1]]):
            x = tf.placeholder(self.LeNet_setting["x"][p[3]],
                               self.LeNet_setting["x"][p[4]],
                               name=self.LeNet_setting["x"][p[0]])
            tf.add_to_collection("model"+self.LeNet_setting["x"][p[2]], x)
        with tf.name_scope(self.LeNet_setting["y"][p[1]]):
            y = tf.placeholder(self.LeNet_setting["y"][p[3]],
                               self.LeNet_setting["y"][p[4]],
                               name=self.LeNet_setting["y"][p[0]])
            tf.add_to_collection("model"+self.LeNet_setting["y"][p[2]], y)
        self.x, self.y = x, y

    def translearning_model(self):
        # Parameters Setting
        p = self.para_list

        # Layer Setting
        layer = NL.conv_layer_uint(self.x,
                                   self.LeNet_setting[0][p[4]],
                                   self.LeNet_setting[0][p[5]],
                                   self.LeNet_setting[0][p[6]],
                                   self.LeNet_setting[0][p[7]],
                                   input_dim="2d",
                                   weights_setting=None,
                                   activation_func=self.LeNet_setting[0][p[8]],
                                   var_scope=self.LeNet_setting[0][p[0]],
                                   layer_name=self.LeNet_setting[0][p[1]],
                                   collection_name=self.LeNet_setting[0][p[2]])

        layer = NL.pooling_layer_uint(layer,
                                      self.LeNet_setting[1][p[4]],
                                      self.LeNet_setting[1][p[6]],
                                      self.LeNet_setting[1][p[7]],
                                      input_dim="2d",
                                      pooling_type="MAX",
                                      layer_name=self.LeNet_setting[1][p[1]],
                                      collection_name=self.LeNet_setting[1][p[2]])
        layer = NL.conv_layer_uint(layer,
                                   self.LeNet_setting[2][p[4]],
                                   self.LeNet_setting[2][p[5]],
                                   self.LeNet_setting[2][p[6]],
                                   self.LeNet_setting[2][p[7]],
                                   input_dim="2d",
                                   weights_setting=None,
                                   activation_func=self.LeNet_setting[2][p[8]],
                                   var_scope=self.LeNet_setting[2][p[0]],
                                   layer_name=self.LeNet_setting[2][p[1]],
                                   collection_name=self.LeNet_setting[2][p[2]])
        layer = NL.pooling_layer_uint(layer,
                                      self.LeNet_setting[3][p[4]],
                                      self.LeNet_setting[3][p[6]],
                                      self.LeNet_setting[3][p[7]],
                                      input_dim="2d",
                                      pooling_type="MAX",
                                      layer_name=self.LeNet_setting[3][p[1]],
                                      collection_name=self.LeNet_setting[3][p[2]])
        layer = NL.flat_layer(layer, axis0_fixed=None)
        layer = NL.dense_layer_uint(layer,
                                    self.LeNet_setting[5][p[4]],
                                    self.LeNet_setting[5][p[5]],
                                    activation_func=self.LeNet_setting[5][p[8]],
                                    var_scope=self.LeNet_setting[5][p[0]],
                                    layer_name=self.LeNet_setting[5][p[1]],
                                    collection_name=self.LeNet_setting[5][p[2]])
        layer = NL.dense_layer_uint(layer,
                                    self.LeNet_setting[6][p[4]],
                                    self.LeNet_setting[6][p[5]],
                                    activation_func=self.LeNet_setting[6][p[8]],
                                    var_scope=self.LeNet_setting[6][p[0]],
                                    layer_name=self.LeNet_setting[6][p[1]],
                                    collection_name=self.LeNet_setting[6][p[2]])

        logits = NL.dense_layer_uint(layer,
                                     self.LeNet_setting[7][p[4]],
                                     self.LeNet_setting[7][p[5]],
                                     activation_func=self.LeNet_setting[7][p[8]],
                                     var_scope=self.LeNet_setting[7][p[0]],
                                     layer_name=self.LeNet_setting[7][p[1]],
                                     collection_name=self.LeNet_setting[7][p[2]])

        return logits

    def tf_module_model_default(self, training_flag=True):
        # Parameters Setting
        p = self.para_list

        """
        0: {p[0]: "conv1", p[1]: "conv1", p[2]: "conv1", p[3]: None, p[4]: [5, 5, 1, 6],
                             p[5]: [6], p[6]: [1, 1, 1, 1], p[7]: "SAME", p[8]: tf.nn.relu},
        1: {p[0]: "pool1", p[1]: "pool1", p[2]: "pool1", p[3]: None, p[4]: [1, 2, 2, 1],
                             p[5]: None, p[6]: [1, 2, 2, 1], p[7]: "VALID", p[8]: None},
        2: {p[0]: "conv2", p[1]: "conv2", p[2]: "conv2", p[3]: None, p[4]: [5, 5, 6, 16],
                             p[5]: [16], p[6]: [1, 1, 1, 1], p[7]: "VALID", p[8]: tf.nn.relu},
        3: {p[0]: "pool2", p[1]: "pool2", p[2]: "pool2", p[3]: None, p[4]: [1, 2, 2, 1],
                             p[5]: None, p[6]: [1, 2, 2, 1], p[7]: "VALID", p[8]: None},
        4: {p[0]: "flatten1", p[1]: "flatten", p[2]: "flatten", p[3]: None, p[4]: None,
                             p[5]: None, p[6]: None, p[7]: None, p[8]: None},
        5: {p[0]: "dense1", p[1]: "dense1", p[2]: "dense1", p[3]: None, p[4]: [400, 120],
                             p[5]: [120], p[6]: None, p[7]: None, p[8]: tf.nn.relu},
        6: {p[0]: "dense2", p[1]: "dense2", p[2]: "dense2", p[3]: None, p[4]: [120, 84],
                             p[5]: [84], p[6]: None, p[7]: None, p[8]: tf.nn.relu},
        7: {p[0]: "dense3", p[1]: "dense3", p[2]: "dense3", p[3]: None, p[4]: [84, 10],
                             p[5]: [10], p[6]: None,p[7]: None,  p[8]: None}}
        """

        # Layer Setting
        #print(self.x)
        layer = NL.conv_layer(self.x,
                              6,
                              5,
                              1,
                              batch_norm_decay=0.85,
                              batch_norm_epsilon=1e-05,
                              batch_norm_flag=True,
                              dropout_rate = 0,
                              activation_para=None,
                              padding_type="SAME",
                              training_flag=training_flag,
                              activation_func=self.LeNet_setting[0][p[8]])
        #print(layer)

        layer = NL.pooling_layer_uint(layer,
                                      self.LeNet_setting[1][p[4]],
                                      self.LeNet_setting[1][p[6]],
                                      self.LeNet_setting[1][p[7]],
                                      input_dim="2d",
                                      pooling_type="MAX",
                                      layer_name=self.LeNet_setting[1][p[1]],
                                      collection_name=self.LeNet_setting[1][p[2]])
        #print(layer)

        layer = NL.conv_layer(layer,
                              16,
                              5,
                              1,
                              batch_norm_decay=0.85,
                              batch_norm_epsilon=1e-05,
                              batch_norm_flag=True,
                              dropout_rate = 0,
                              activation_para=None,
                              padding_type="VALID",
                              training_flag=training_flag,
                              activation_func=self.LeNet_setting[2][p[8]])
        #print(layer)

        layer = NL.pooling_layer_uint(layer,
                                      self.LeNet_setting[3][p[4]],
                                      self.LeNet_setting[3][p[6]],
                                      self.LeNet_setting[3][p[7]],
                                      input_dim="2d",
                                      pooling_type="MAX",
                                      layer_name=self.LeNet_setting[3][p[1]],
                                      collection_name=self.LeNet_setting[3][p[2]])
        #print(layer)

        layer = NL.flat_layer(layer, axis0_fixed=None)
        #print(layer)
        layer = NL.dense_layer(layer,
                               120,
                               dropout_rate = 0,
                               training_flag = True,
                               kernel_regularizer = None,
                               activity_regularizer = None,
                               bias_regularizer = None,
                               use_bais_flag=True,
                               activation_func = self.LeNet_setting[5][p[8]],
                               collection_name = self.LeNet_setting[5][p[2]])

        #print(layer)
        layer = NL.dense_layer(layer,
                               84,
                               dropout_rate = 0,
                               training_flag=True,
                               kernel_regularizer=None,
                               activity_regularizer=None,
                               bias_regularizer=None,
                               use_bais_flag=True,
                               activation_func=self.LeNet_setting[6][p[8]],
                               collection_name=self.LeNet_setting[6][p[2]])

        #print(layer)
        logits = NL.dense_layer(layer,
                               10,
                               dropout_rate=0,
                               training_flag=True,
                               kernel_regularizer=None,
                               activity_regularizer=None,
                               bias_regularizer=None,
                               use_bais_flag=False,
                               activation_func=self.LeNet_setting[7][p[8]],
                               collection_name=self.LeNet_setting[7][p[2]])
        print(logits.name)

        """
        logits = NL.dense_layer_uint(layer,
                                     self.LeNet_setting[7][p[4]],
                                     self.LeNet_setting[7][p[5]],
                                     activation_func=self.LeNet_setting[7][p[8]],
                                     var_scope=self.LeNet_setting[7][p[0]],
                                     layer_name=self.LeNet_setting[7][p[1]],
                                     collection_name=self.LeNet_setting[7][p[2]])
        """


        #print(logits)
        return logits

    def model_defualt(self):
        # Parameters Setting
        p = self.para_list

        # Layer Setting
        #print(self.x)
        layer = NL.conv_layer_uint(self.x,
                                   self.LeNet_setting[0][p[4]],
                                   self.LeNet_setting[0][p[5]],
                                   self.LeNet_setting[0][p[6]],
                                   self.LeNet_setting[0][p[7]],
                                   input_dim="2d",
                                   weights_setting=None,
                                   activation_func=self.LeNet_setting[0][p[8]],
                                   var_scope=self.LeNet_setting[0][p[0]],
                                   layer_name=self.LeNet_setting[0][p[1]],
                                   collection_name=self.LeNet_setting[0][p[2]])
        #print(layer)

        layer = NL.pooling_layer_uint(layer,
                                      self.LeNet_setting[1][p[4]],
                                      self.LeNet_setting[1][p[6]],
                                      self.LeNet_setting[1][p[7]],
                                      input_dim="2d",
                                      pooling_type="MAX",
                                      layer_name=self.LeNet_setting[1][p[1]],
                                      collection_name=self.LeNet_setting[1][p[2]])
        #print(layer)

        layer = NL.conv_layer_uint(layer,
                                   self.LeNet_setting[2][p[4]],
                                   self.LeNet_setting[2][p[5]],
                                   self.LeNet_setting[2][p[6]],
                                   self.LeNet_setting[2][p[7]],
                                   input_dim="2d",
                                   weights_setting=None,
                                   activation_func=self.LeNet_setting[2][p[8]],
                                   var_scope=self.LeNet_setting[2][p[0]],
                                   layer_name=self.LeNet_setting[2][p[1]],
                                   collection_name=self.LeNet_setting[2][p[2]])
        #print(layer)

        layer = NL.pooling_layer_uint(layer,
                                      self.LeNet_setting[3][p[4]],
                                      self.LeNet_setting[3][p[6]],
                                      self.LeNet_setting[3][p[7]],
                                      input_dim="2d",
                                      pooling_type="MAX",
                                      layer_name=self.LeNet_setting[3][p[1]],
                                      collection_name=self.LeNet_setting[3][p[2]])
        #print(layer)

        layer = NL.flat_layer(layer, axis0_fixed=None)
        #print(layer)
        layer = NL.dense_layer_uint(layer,
                                    self.LeNet_setting[5][p[4]],
                                    self.LeNet_setting[5][p[5]],
                                    activation_func=self.LeNet_setting[5][p[8]],
                                    var_scope=self.LeNet_setting[5][p[0]],
                                    layer_name=self.LeNet_setting[5][p[1]],
                                    collection_name=self.LeNet_setting[5][p[2]])
        #print(layer)
        layer = NL.dense_layer_uint(layer,
                                    self.LeNet_setting[6][p[4]],
                                    self.LeNet_setting[6][p[5]],
                                    activation_func=self.LeNet_setting[6][p[8]],
                                    var_scope=self.LeNet_setting[6][p[0]],
                                    layer_name=self.LeNet_setting[6][p[1]],
                                    collection_name=self.LeNet_setting[6][p[2]])
        #print(layer)
        logits = NL.dense_layer_uint(layer,
                                     self.LeNet_setting[7][p[4]],
                                     self.LeNet_setting[7][p[5]],
                                     activation_func=self.LeNet_setting[7][p[8]],
                                     var_scope=self.LeNet_setting[7][p[0]],
                                     layer_name=self.LeNet_setting[7][p[1]],
                                     collection_name=self.LeNet_setting[7][p[2]])
        #print(layer)
        return logits


