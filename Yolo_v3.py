# -*- coding: utf-8 -*-
"""
Note:
Darknet 53 Layer
Inputs → Conv2D Layer → Residual Block 1  → Residual Block 2  → Residual Block 3  → Residual Block 4 → Residual Block 5
                                                                                                                   ↓                               ↓                              ↓
                                                                                                                   ↓                               ↓                 Conv2D Block 1024 → Detection Layer 1
                                                                                                                   ↓                               ↓                              ↓
                                                                                                                   ↓                        Concat Block 1  ← Conv2D + UpSampling 2D Layer1
                                                                                                                   ↓                                ↓
                                                                                                                   ↓                     Conv2D Block 256 → Detection Layer 2
                                                                                                                   ↓                                 ↓
                                                                                                            Concat Block 2  ←  Conv2D + UpSampling 2D Layer2
                                                                                                                   ↓
                                                                                                        Conv2D Block 128 → Detection Layer 3



@author: CY Peng

        Note:
        @xy_loss
        OK for Convengence, Original
        xy_loss = ((y_box_centers[:, :, 0:1] - yp_box_centers[:, :, 0:1]) ** 2 +
                (y_box_centers[:, :, 1:2] - yp_box_centers[:, :, 1:2]) ** 2) *
                box_loss_scale[:, :,0:1] * LambdaCoord[:, :, 0:1]

        OK for Convengence
        xy_loss = box_loss_scale[:, :, 0:1] * LambdaCoord[:, :, 0:1] * tf.losses.absolute_difference(y_box_centers,
                                                                                                     yp_box_centers)

        Don't Work for Convengence
        xy_loss = box_loss_scale[:, :,0:1] * LambdaCoord[:, :, 0:1]*tf.losses.mean_squared_error(y_box_centers,
                                                                                                 yp_box_centers)
        xy_loss = box_loss_scale[:, :, 0:1] * LambdaCoord[:, :, 0:1] * tf.losses.absolute_difference(y_box_centers,
                                                                                                     yp_box_centers)

        Difficult for Convengence
        box_loss_scale[:, :,0:1] * LambdaCoord[:, :, 0:1]*\
                  tf.nn.sigmoid_cross_entropy_with_logits(labels= y_box_centers[:, :, 0:],
                                                          logits= yp_box_centers[:, :, 0:])

        @wh_loss
        OK for Convengence, Original
        wh_loss = ((tf.maximum(y_box_shapes[:, :, 0:1], 0)**0.5 - tf.maximum(yp_box_shapes[:, :, 0:1], 0)**0.5) ** 2 +
                  (tf.maximum(y_box_shapes[:, :, 1:2], 0)**0.5 - tf.maximum(yp_box_shapes[:, :, 1:2], 0)**0.5) ** 2) * \
                 box_loss_scale[:, :,0:1] * LambdaCoord[:, :, 0:1]

        OK for Convengence
        wh_loss = box_loss_scale[:, :, 0:1] * LambdaCoord[:, :, 0:1] * tf.losses.absolute_difference(y_box_shapes, yp_box_shapes)

        Don't Work for Convengence
        wh_loss = box_loss_scale[:, :,0:1] * LambdaCoord[:, :, 0:1]*tf.losses.mean_squared_error(y_box_shapes,
                                                                                                 yp_box_shapes)
        wh_loss = box_loss_scale[:, :, 0:1] * LambdaCoord[:, :, 0:1] * tf.losses.absolute_difference(y_box_shapes, yp_box_shapes)

        Difficult for Convengence
        wh_loss = box_loss_scale[:, :, 0:1] * LambdaCoord[:, :, 0:1] * tf.losses.log_loss(y_box_shapes, yp_box_shapes)

        @class loss
        OK for Convengence, Original
        conf_pos_loss = conf_pos_mask * (y_confidence[:, :, 0:1]-yp_confidence[:, :, 0:1])**2
        conf_neg_loss = conf_neg_mask * (y_confidence[:, :, 0:1]-yp_confidence[:, :, 0:1])**2

        Slow for Convengence
        conf_pos_loss = conf_pos_mask * tf.nn.sigmoid_cross_entropy_with_logits(labels=y_confidence[:, :, 0:],
                                                                                logits=yp_confidence[:, :, 0:])
        conf_neg_loss = conf_neg_mask * tf.nn.sigmoid_cross_entropy_with_logits(labels=y_confidence[:, :, 0:],
                                                                                logits=yp_confidence[:, :, 0:])

        Don't Work for Convengence
        conf_pos_loss = conf_pos_mask * tf.losses.absolute_difference(y_confidence[:, :, 0:],
                                                                      yp_confidence[:, :, 0:])
        conf_neg_loss = conf_neg_mask * tf.losses.absolute_difference(y_confidence[:, :, 0:],
                                                                      yp_confidence[:, :, 0:])

        @ Class Loss
                OK for Convengence, Original
        class_loss = y_confidence[:, :, 0:1] * (y_classes[:, :, 0:1]-yp_classes[:, :, 0:1])**2

        Slow for Convengence
        class_loss = y_confidence[:, :, 0:1] *tf.nn.sigmoid_cross_entropy_with_logits(labels= y_classes[:, :, 0:],
                                                logits=yp_classes[:, :, 0:])

        Don't Work for Convengence
        class_loss = y_confidence[:, :, 0:1] *tf.losses.absolute_difference(y_classes[:, :, 0:],
                                                                            yp_classes[:, :, 0:])

DataVer  - Author - Note
2019/05/29      CY   Yolo Model Set
2019/06/16    CY     Training Loss Check, Bug Fixed
2019/06/18    CY     Training Loss Issues
"""
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import tensorflow as tf
import NetworkLib as NL
import numpy as np

class Yolo_v3:
    def __init__(self, batch_size=64, n_classes = 80, model_size =(416, 416), transfer_learning_flag = False,
                 model_name="yolo_v3", learning_rate = 0.001):

        self.batch_size = batch_size
        self.model_name = model_name
        self.n_classes = n_classes
        self.model_size = model_size
        self.transfer_learning_flag = transfer_learning_flag
        self.anchors = [(10, 13), (16, 30), (33, 23),
                        (30, 61), (62, 45), (59, 119),
                        (116, 90), (156, 198), (373, 326)]
        self.model_input()
        self.learning_rate_value = learning_rate
        self.y1pred, self.y2pred, self.y3pred, self.ypred = self.model()

    def set_tran_variable_scope(self, var_list = None):
        self.tran_variable_list = NL.get_multi_training_variable_list(var_list=var_list, name_scope=self.model_name)
        #print(self.tran_variable_list)
        self.__estimation__(var_list = var_list)
        self.__backpropagation__()

    def __estimation__(self, var_list = None):
        flag = [0, 0, 0]
        y_xy_loss, y_wh_loss, y_pos_conf_loss, y_neg_conf_loss, y_class_loss = [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]
        y_true_box_precision, y_true_box_recall, y_true_box_iou = [0, 0, 0], [0, 0, 0], [0, 0, 0]
        y_pos_num, y_neg_num = [0, 0, 0], [0, 0, 0]
        pos_target_num, neg_target_num = 0, 0

        if "yolo_v3/new_conv_layer58" in var_list:
            flag[0] = 1
        if "yolo_v3/new_conv_layer66" in var_list:
            flag[1] = 1
        if "yolo_v3/new_conv_layer74" in var_list:
            flag[2] = 1

        with tf.name_scope("loss_operation"):
            # Training Stage Operation
            if flag[0]:
                y1array = self.label2pred(self.y1, self.anchors[6:9])
                y_xy_loss[0], y_wh_loss[0], y_pos_conf_loss[0], y_neg_conf_loss[0], y_class_loss[0], \
                y_true_box_precision[0], y_true_box_recall[0], y_true_box_iou[0], \
                y_pos_num[0], y_neg_num[0] = \
                    self.loss_count(y1array, self.y1pred, self.anchors[6:9], grid_shape=(13, 13))

            if flag[1]:
                y2array = self.label2pred(self.y2, self.anchors[3:6])
                y_xy_loss[1], y_wh_loss[1], y_pos_conf_loss[1], y_neg_conf_loss[1], y_class_loss[1], \
                y_true_box_precision[1], y_true_box_recall[1], y_true_box_iou[1], \
                y_pos_num[1], y_neg_num[1] = \
                    self.loss_count(y2array, self.y2pred, self.anchors[3:6], grid_shape=(26, 26))

            if flag[2]:
                y3array = self.label2pred(self.y3, self.anchors[0:3])
                y_xy_loss[2], y_wh_loss[2], y_pos_conf_loss[2], y_neg_conf_loss[2], y_class_loss[2], \
                y_true_box_precision[2], y_true_box_recall[2], y_true_box_iou[2], \
                y_pos_num[2], y_neg_num[2] = \
                    self.loss_count(y3array, self.y3pred, self.anchors[0:3], grid_shape=(52, 52))

            # Mean Calculation
            for idx in range(2):
                pos_target_num += tf.to_float(y_pos_num[idx])
                neg_target_num += tf.to_float(y_neg_num[idx])

            pos_target_denominator = tf.to_float(tf.maximum(pos_target_num, 1))
            neg_target_denominator = tf.to_float(tf.maximum(neg_target_num, 1))
            """
            Note:
            Mean Data: Bad Performance for Convengence 
            """
            xy_loss = np.sum(y_xy_loss)/(pos_target_denominator) #
            wh_loss = np.sum(y_wh_loss)/(pos_target_denominator) #
            conf_pos_loss = np.sum(y_pos_conf_loss)/(pos_target_denominator)
            conf_neg_loss = np.sum(y_neg_conf_loss)/(neg_target_denominator)
            class_loss = np.sum(y_class_loss)/(pos_target_denominator)

            # Calculate the Mean of the Precision/ Recall/ IOU
            precision = np.sum(y_true_box_precision)/(pos_target_denominator)
            recall = np.sum(y_true_box_recall)/(pos_target_denominator)
            iou =np.sum(y_true_box_iou)/(pos_target_denominator)

            # Set the Loss Operation
            self.loss_operation_monitor = tf.convert_to_tensor([xy_loss, wh_loss, conf_pos_loss, conf_neg_loss, class_loss,
                                                                precision, recall, iou, pos_target_num, neg_target_num],
                                                               name="loss_operation_monitor")
            self.loss_operation = class_loss + conf_pos_loss + conf_neg_loss + xy_loss + wh_loss
            tf.add_to_collection("loss_operation", self.loss_operation)
            tf.add_to_collection("loss_operation_monitor", self.loss_operation_monitor)

    def __backpropagation__(self):
        with tf.name_scope("training_operation"):
            """
              learning_rate = tf.train.exponential_decay(self.learning_rate_value,
                                                       staircase=True,
                                                       global_step=1000,
                                                       decay_steps=100,
                                                       decay_rate=0.96)          
            """

            optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate_value)
            training_operation = optimizer.minimize(self.loss_operation,
                                                    var_list=self.tran_variable_list)
            tf.add_to_collection("training_operation", training_operation)

            self.training_operation = training_operation

    def label2pred(self, layer, anchors):
        n_anchors = len(anchors)
        shape = layer.get_shape().as_list()
        grid_shape = shape[1:3]
        inputs = tf.reshape(layer, [-1, n_anchors * grid_shape[0] * grid_shape[1],
                                    5 + self.n_classes])
        y_box_centers, y_box_shapes, y_confidence, y_classes = \
            tf.split(inputs, [2, 2, 1, self.n_classes], axis=-1)
        decode_label = tf.concat([y_box_centers, y_box_shapes,
                                  y_confidence, y_classes], axis=-1)
        return decode_label

    """
        def boxes_iou(self, y_box_xy, y_box_wh, yp_box_xy, yp_box_wh):
        yp_box_left = yp_box_xy[:, :, 0:1] - yp_box_wh[:, :, 0:1] * 0.5
        yp_box_right = yp_box_xy[:, :, 0:1] + yp_box_wh[:, :, 0:1] * 0.5
        yp_box_top = yp_box_xy[:, :, 1:2] - yp_box_wh[:, :, 1:2] * 0.5
        yp_box_bottom = yp_box_xy[:, :, 1:2] + yp_box_wh[:, :, 1:2] * 0.5
        y_box_left = y_box_xy[:, :, 0:1] - y_box_wh[:, :, 0:1] * 0.5
        y_box_right = y_box_xy[:, :, 0:1] + y_box_wh[:, :, 0:1] * 0.5
        y_box_top = y_box_xy[:, :, 1:2] - y_box_wh[:, :, 1:2] * 0.5
        y_box_bottom = y_box_xy[:, :, 1:2] + y_box_wh[:, :, 1:2] * 0.5

        intersect_x_left = tf.maximum(yp_box_left[:, :, 0:1],
                                      y_box_left[:, :, 0:1])
        intersect_x_right = tf.minimum(yp_box_right[:, :, 0:1],
                                       y_box_right[:, :, 0:1])
        intersect_y_top = tf.maximum(yp_box_top[:, :, 0:1],
                                     y_box_top[:, :, 0:1])
        intersect_y_bottom = tf.minimum(yp_box_bottom[:, :, 0:1],
                                        y_box_bottom[:, :, 0:1])

        union_x_left = tf.minimum(yp_box_left[:, :, 0:1],
                                  y_box_left[:, :, 0:1])
        union_x_right = tf.maximum(yp_box_right[:, :, 0:1],
                                   y_box_right[:, :, 0:1])
        union_y_top = tf.minimum(yp_box_top[:, :, 0:1],
                                 y_box_top[:, :, 0:1])
        union_y_bottom = tf.maximum(yp_box_bottom[:, :, 0:1],
                                    y_box_bottom[:, :, 0:1])

        intersect_h = tf.maximum(intersect_y_bottom[:, :, 0:1] - intersect_y_top[:, :, 0:1], 0)
        intersect_w = tf.maximum(intersect_x_right[:, :, 0:1] - intersect_x_left[:, :, 0:1], 0)
        union_h = tf.maximum(union_y_bottom[:, :, 0:1] - union_y_top[:, :, 0:1], 0)
        union_w = tf.maximum(union_x_right[:, :, 0:1] - union_x_left[:, :, 0:1], 0)

        intersect_area = intersect_w[:, :, 0:1] * intersect_h[:, :, 0:1]
        union_area = union_w[:, :, 0:1] * union_h[:, :, 0:1]
        pred_box_area  = yp_box_wh[:, :, 0:1]  * yp_box_wh[:, :, 1:2]
        true_box_area  = y_box_wh[:, :, 0:1]  * y_box_wh[:, :, 1:2]

        #check_area = tf.cast(pred_box_area[:, :, 0:1] + true_box_area[:, :, 0:1] >= intersect_area[:, :, 0:1], tf.float32)
        iou_denominator = tf.maximum((pred_box_area[:, :, 0:1] + true_box_area[:, :, 0:1] - intersect_area[:, :, 0:1]), 1)
        precision_denominator = tf.maximum(pred_box_area[:, :, 0:1], 1)
        recall_denominator = tf.maximum(true_box_area[:, :, 0:1], 1)
        giou_denominator = tf.maximum((union_area[:, :, 0:1]), 1)

        check_para = tf.cast(pred_box_area[:, :, 0:1] + true_box_area[:, :, 0:1] >= intersect_area[:, :, 0:1], tf.float32)
        iou = (intersect_area[:, :, 0:1] / iou_denominator)*check_para
        precision = (intersect_area[:, :, 0:1]/precision_denominator)*check_para
        recall = (intersect_area[:, :, 0:1]/recall_denominator)*check_para
        giou = 1-(iou - (union_area[:, :, 0:1]-intersect_area[:, :, 0:1])/giou_denominator[:, :, 0:1])

        # Check
        iou = tf.maximum(iou, 0)
        precision = tf.maximum(precision, 0)
        recall =tf.maximum(recall, 0)
        giou = tf.maximum(giou, 0)
        iou = tf.minimum(iou, 1)
        precision = tf.minimum(precision, 1)
        recall =tf.minimum(recall, 1)
        giou = tf.minimum(giou, 2)

        return precision, recall, iou, giou
    """


    def boxes_iou(self, box_obj, p_box_obj):
        (box_x, box_y, box_w, box_h) = box_obj
        (p_box_x, p_box_y, p_box_w, p_box_h) = p_box_obj
        p_box_left = p_box_x - p_box_w * 0.5
        p_box_right = p_box_x+ p_box_w * 0.5
        p_box_top = p_box_x - p_box_h * 0.5
        p_box_bottom = p_box_x + p_box_h * 0.5
        box_left = box_x - box_w * 0.5
        box_right = box_x+ box_w * 0.5
        box_top = box_x - box_h * 0.5
        box_bottom = box_x + box_h * 0.5

        intersect_x_left = tf.maximum(p_box_left,
                                      box_left)
        intersect_x_right = tf.minimum(p_box_right,
                                       box_right)
        intersect_y_top = tf.maximum(p_box_top,
                                     box_top)
        intersect_y_bottom = tf.minimum(p_box_bottom,
                                        box_bottom)

        union_x_left = tf.minimum(p_box_left,
                                  box_left)
        union_x_right = tf.maximum(p_box_right,
                                   box_right)
        union_y_top = tf.minimum(p_box_top,
                                 box_top)
        union_y_bottom = tf.maximum(p_box_bottom,
                                    box_bottom)

        intersect_h = tf.maximum(intersect_y_bottom - intersect_y_top, 0)
        intersect_w = tf.maximum(intersect_x_right - intersect_x_left, 0)
        union_h = tf.maximum(union_y_bottom - union_y_top, 0)
        union_w = tf.maximum(union_x_right - union_x_left, 0)

        intersect_area = intersect_w * intersect_h
        union_area = union_w * union_h
        pred_box_area = p_box_w * p_box_h
        true_box_area = box_w * box_h

        iou_denominator = tf.maximum((pred_box_area + true_box_area - intersect_area),1)
        precision_denominator = tf.maximum(pred_box_area, 1)
        recall_denominator = tf.maximum(true_box_area, 1)
        giou_denominator = tf.maximum(union_area, 1)

        iou = (intersect_area / iou_denominator)
        precision = (intersect_area / precision_denominator)
        recall = (intersect_area / recall_denominator)
        giou = 1 - (iou - (union_area - intersect_area) / giou_denominator)

        return precision, recall, iou, giou

    def get_effect_y(self, y_box_centers, y_box_shapes, yp_box_centers, yp_box_shapes, pos_effective_matrix):
        y_box_x = tf.gather_nd(y_box_centers[:, :, 0:1], pos_effective_matrix)
        y_box_y = tf.gather_nd(y_box_centers[:, :, 1:2], pos_effective_matrix)
        y_box_w = tf.gather_nd(y_box_shapes[:, :, 0:1], pos_effective_matrix)
        y_box_h = tf.gather_nd(y_box_shapes[:, :, 1:2], pos_effective_matrix)
        yp_box_x = tf.gather_nd(yp_box_centers[:, :, 0:1], pos_effective_matrix)
        yp_box_y = tf.gather_nd(yp_box_centers[:, :, 1:2], pos_effective_matrix)
        yp_box_w = tf.gather_nd(yp_box_shapes[:, :, 0:1], pos_effective_matrix)
        yp_box_h = tf.gather_nd(yp_box_shapes[:, :, 1:2], pos_effective_matrix)

        y_box = (y_box_x, y_box_y, y_box_w, y_box_h)
        yp_box = (yp_box_x, yp_box_y, yp_box_w, yp_box_h)
        return y_box, yp_box

    def get_effect_class_confidence(self, y_confidence, y_classes,
                                         yp_confidence, yp_classes,
                                         pos_effective_matrix, neg_effective_matrix):
        y_pos_confidence = tf.gather_nd(y_confidence[:, :, 0:1], pos_effective_matrix)
        y_pos_classes = tf.gather_nd(y_classes[:, :, 0:], pos_effective_matrix)
        yp_pos_confidence = tf.gather_nd(yp_confidence[:, :, 0:1], pos_effective_matrix)
        yp_pos_classes = tf.gather_nd(yp_classes[:, :, 0:], pos_effective_matrix)
        y_neg_confidence = tf.gather_nd(y_confidence[:, :, 0:1], neg_effective_matrix)
        y_neg_classes = tf.gather_nd(y_classes[:, :, 0:], neg_effective_matrix)
        yp_neg_confidence = tf.gather_nd(yp_confidence[:, :, 0:1], neg_effective_matrix)
        yp_neg_classes = tf.gather_nd(yp_classes[:, :, 0:], neg_effective_matrix)
        y_class_confindence = (y_pos_confidence, y_pos_classes, y_neg_confidence, y_neg_classes)
        yp_class_confindence = (yp_pos_confidence, yp_pos_classes, yp_neg_confidence, yp_neg_classes)

        return y_class_confindence, yp_class_confindence


    def get_effect_boxes_iou(self, y_box_centers, y_box_shapes, yp_box_centers, yp_box_shapes, pos_effective_matrix):
        # Data Processing - Original Size
        # y_box_centers = y_box_centers * tf.to_float(y_confidence) + (x_y_offset / image_ratio) * tf.to_float(
        #    1 - y_confidence)
        # y_box_shapes = y_box_shapes * tf.to_float(y_confidence) + (tf.to_float(anchors)) * tf.to_float(1 - y_confidence)
        y_box, yp_box = self.get_effect_y(y_box_centers, y_box_shapes, yp_box_centers, yp_box_shapes,
                                          pos_effective_matrix)

        # loss iou
        precision, recall, iou, giou = self.boxes_iou(y_box,
                                                      yp_box)
        # best_iou = tf.expand_dims(tf.reduce_max(iou[:,:,0:], axis=2), -1)
        return precision, recall, iou, giou

    def get_obj_effect_coord_loss(self, y_box_centers, y_box_shapes, yp_box_centers, yp_box_shapes, pos_effective_matrix):

        y_box, yp_box = self.get_effect_y(y_box_centers, y_box_shapes, yp_box_centers, yp_box_shapes,
                                          pos_effective_matrix)
        """
        y_class_confindence, yp_class_confindence = self.get_effect_class_confidence(y_confidence, y_classes,
                                                                                     yp_confidence, yp_classes,
                                                                                     pos_effective_matrix,
                                                                                     neg_effective_matrix)
        """

        # coordinate loss
        LambdaCoord = 5.5
        box_loss_scale = 2. - (y_box[3] / self.model_size[1]) * (y_box[2] / self.model_size[0])
        xy_loss = box_loss_scale * LambdaCoord* (tf.abs(y_box[0] - yp_box[0]) + tf.abs(y_box[1] - yp_box[1]))
        wh_loss = box_loss_scale * LambdaCoord * (tf.abs(y_box[2] - yp_box[2]) + tf.abs(y_box[3] - yp_box[3]))


        return xy_loss, wh_loss #, conf_pos_loss, conf_neg_loss, class_loss

    def get_noobj_effect_loss(self, y_confidence, y_classes, yp_confidence, yp_classes,
                                    pos_effective_matrix, neg_effective_matrix):
        y_class_confindence, yp_class_confindence = self.get_effect_class_confidence(y_confidence, y_classes,
                                                                                     yp_confidence, yp_classes,
                                                                                     pos_effective_matrix,
                                                                                     neg_effective_matrix)
        LambdaNoObj = 0.5  # * tf.cast(best_iou < iou_thread, tf.float32)
        conf_neg_mask = LambdaNoObj
        conf_neg_loss = conf_neg_mask * tf.nn.sigmoid_cross_entropy_with_logits(labels=y_class_confindence[2],
                                                                                logits=yp_class_confindence[2])

        # class loss
        class_loss = tf.nn.sigmoid_cross_entropy_with_logits(labels=y_classes[1],
                                                             logits=yp_classes[1])

        return 0, 0, 0, conf_neg_loss, class_loss



    def loss_count(self, yarray, yparray, anchors, grid_shape = (13, 13), image_shape = (416, 416), iou_thread = 0.5):
        # usage array
        n_anchors = len(anchors)

        # split the array to each column
        y_box_centers, y_box_shapes, y_confidence, y_classes = \
            tf.split(yarray, [2, 2, 1, self.n_classes], axis=-1)

        yp_box_centers, yp_box_shapes, yp_confidence, yp_classes = \
            tf.split(yparray, [2, 2, 1, self.n_classes], axis=-1)

        pos_effective_matrix = tf.where(tf.equal(y_confidence, 1))
        neg_effective_matrix = tf.where(tf.equal(1-y_confidence, 1))

        pos_shape = tf.shape(pos_effective_matrix)
        neg_shape = tf.shape(neg_effective_matrix)
        pos_num = pos_shape[0]
        neg_num = neg_shape[0]
        #flag = tf.cond(pos_num > 0, bool(1), bool(0))

        #if tf.greater(pos_num, 0):
        precision, recall, iou, giou = self.get_effect_boxes_iou(y_box_centers, y_box_shapes, yp_box_centers,
                                                                 yp_box_shapes, pos_effective_matrix)

        # base boxes offset
        image_ratio = (grid_shape[0] / image_shape[0], grid_shape[1] / image_shape[1])
        x = tf.range(grid_shape[0], dtype=tf.float32)
        y = tf.range(grid_shape[1], dtype=tf.float32)
        x_offset, y_offset = tf.meshgrid(x, y)
        x_offset = tf.reshape(x_offset, (-1, 1))
        y_offset = tf.reshape(y_offset, (-1, 1))
        x_y_offset = tf.concat([x_offset, y_offset], axis=-1)
        x_y_offset = tf.tile(x_y_offset, [1, n_anchors])
        x_y_offset = tf.reshape(x_y_offset, [1, -1, 2])
        # base box shape
        anchors = tf.tile(anchors, [grid_shape[0] * grid_shape[1], 1])
        anchors = tf.reshape(anchors, [1, -1, 2])

        # Data Processing - Yolo Size
        """
        Note:
        Original: (Some issue in custom training)
        Don't Work for Convengence, Custom Training
        y_box_shapes = tf.log(y_box_shapes/tf.to_float(anchors))
        yp_box_shapes = tf.log(yp_box_shapes/tf.to_float(anchors))

        OK for Convengence (Power Method)
        y_box_shapes = (y_box_shapes/tf.to_float(anchors))** (1 / 3) / 2
        yp_box_shapes = (yp_box_shapes/tf.to_float(anchors))** (1 / 3) / 2
        """
        self.test1 = tf.gather_nd(y_box_shapes[:, :, 0:1], pos_effective_matrix)
        self.test2 = tf.gather_nd(yp_box_shapes[:, :, 0:1], pos_effective_matrix)
        self.test3 = tf.gather_nd(y_box_shapes[:, :, 1:2], pos_effective_matrix)
        self.test4 = tf.gather_nd(yp_box_shapes[:, :, 1:2], pos_effective_matrix)
        self.test5 = tf.gather_nd(y_box_centers[:, :, 0:1], pos_effective_matrix)
        self.test6 = tf.gather_nd(yp_box_centers[:, :, 0:1], pos_effective_matrix)
        self.test7 = tf.gather_nd(y_box_centers[:, :, 1:2], pos_effective_matrix)
        self.test8 = tf.gather_nd(yp_box_centers[:, :, 1:2], pos_effective_matrix)
        y_box_shapes = (y_box_shapes / tf.to_float(anchors))  # ** (1 / 3) / 2
        yp_box_shapes = (yp_box_shapes / tf.to_float(anchors))  # ** (1 / 3) / 2
        y_box_centers = y_box_centers * image_ratio - x_y_offset
        yp_box_centers = yp_box_centers * image_ratio - x_y_offset

        xy_loss, wh_loss = self.get_obj_effect_coord_loss(
            y_box_centers, y_box_shapes, yp_box_centers, yp_box_shapes, pos_effective_matrix)

        # confidence loss
        LambdaNoObj = 0.5  # * tf.cast(best_iou < iou_thread, tf.float32)
        # conf_pos_mask = iou #iou # *tf.cast(iou > iou_thread, tf.float32)
        conf_neg_mask = LambdaNoObj
        conf_pos_err = iou * tf.gather_nd(y_confidence[:, :, 0:1], pos_effective_matrix) - tf.gather_nd(
            yp_confidence[:, :, 0:1], pos_effective_matrix)
        conf_neg_err = y_confidence[:, :, 0:1] - yp_confidence[:, :, 0:1]  # (1-y_confidence[:, :, 0:1])*
        conf_neg_err = conf_neg_err # * tf.cast(tf.abs(conf_neg_err) > 0.5, tf.float32)
        conf_pos_loss = conf_pos_err ** 2
        conf_neg_loss = conf_neg_mask * tf.gather_nd(conf_neg_err ** 2, neg_effective_matrix)
        # tf.reduce_sum(conf_neg_mask * tf.cast(iou < 0.7, tf.float32) * tf.gather_nd(((1-y_confidence[:, :, 0:1]) - yp_confidence[:, :, 0:1]) ** 2, pos_effective_matrix))

        # class loss
        class_loss = y_confidence[:, :, 0:1]*tf.reduce_sum(((y_classes[:, :, 0:] - yp_classes[:, :, 0:]) ** 2), axis=-1, keepdims=True)
        #print(class_loss)

        xy_sum_loss = tf.reduce_sum(xy_loss)
        wh_sum_loss = tf.reduce_sum(wh_loss)
        conf_pos_sum_loss = tf.reduce_sum(conf_pos_loss)
        conf_neg_sum_loss = tf.reduce_sum(conf_neg_loss)
        class_sum_loss = tf.reduce_sum(class_loss)
        true_box_sum_precision = tf.reduce_sum(precision)
        true_box_sum_recall = tf.reduce_sum(recall)
        true_box_sum_iou = tf.reduce_sum(iou)
        self.test9 = [xy_sum_loss, wh_sum_loss, conf_pos_loss, conf_neg_loss, class_sum_loss]
        self.test10 = [iou, recall, precision]


        return xy_sum_loss, wh_sum_loss, conf_pos_sum_loss, conf_neg_sum_loss, class_sum_loss, \
               true_box_sum_precision, true_box_sum_recall, true_box_sum_iou, \
               pos_num, neg_num


    def darknet53(self, input, training_flag=True):
        with tf.name_scope("Darknet53"):
            layer = NL.conv_layer(input, 32, 3, 1, training_flag=training_flag,
                                  activation_func=tf.nn.leaky_relu)

            layer = NL.conv_layer(self.yolo_padding(layer, 3), 64, 3, 2, training_flag=training_flag,
                                  padding_type="VALID", activation_func=tf.nn.leaky_relu)

            layer = NL.ResNet_layer(layer, 32, 1, 1, 3, 1,
                                    training_flag=training_flag,
                                    activation_func1=tf.nn.leaky_relu, activation_func2=tf.nn.leaky_relu)

            layer = NL.conv_layer(self.yolo_padding(layer, 3), 128, 3, 2, training_flag=training_flag,
                                  padding_type="VALID", activation_func=tf.nn.leaky_relu)

            for _ in range(2):
                layer = NL.ResNet_layer(layer, 64, 1, 1, 3, 1,
                                        training_flag=training_flag,
                                        activation_func1=tf.nn.leaky_relu, activation_func2=tf.nn.leaky_relu)

            layer = NL.conv_layer(self.yolo_padding(layer, 3), 256, 3, 2, training_flag=training_flag,
                                  padding_type="VALID", activation_func=tf.nn.leaky_relu)

            for _ in range(8):
                layer = NL.ResNet_layer(layer, 128, 1, 1, 3, 1,
                                        training_flag=training_flag,
                                        activation_func1=tf.nn.leaky_relu, activation_func2=tf.nn.leaky_relu)

            route1 = layer

            layer = NL.conv_layer(self.yolo_padding(layer, 3), 512, 3, 2, training_flag=training_flag,
                                  padding_type="VALID", activation_func=tf.nn.leaky_relu)

            for _ in range(8):
                layer = NL.ResNet_layer(layer, 256, 1, 1, 3, 1,
                                        training_flag=training_flag,
                                        activation_func1=tf.nn.leaky_relu, activation_func2=tf.nn.leaky_relu)
            route2 = layer
            layer = NL.conv_layer(self.yolo_padding(layer, 3), 1024, 3, 2, training_flag=training_flag,
                                  padding_type="VALID", activation_func=tf.nn.leaky_relu)

            for _ in range(4):
                layer = NL.ResNet_layer(layer, 512, 1, 1, 3, 1,
                                        training_flag=training_flag,
                                        activation_func1=tf.nn.leaky_relu, activation_func2=tf.nn.leaky_relu)

            return route1, route2, layer

    def yolo_padding(self, input, kernel_size):
        pad_total = kernel_size - 1
        pad_beg = pad_total // 2
        pad_end = pad_total - pad_beg
        padding_layer = tf.pad(input, [[0, 0], [pad_beg, pad_end],
                               [pad_beg, pad_end], [0, 0]])
        return padding_layer

    def conv_block(self, input, filter_num, training_flag=True):
        with tf.name_scope("ConvBlock"):
            layer = NL.ResNet_layer(input, filter_num, 1, 1, 3, 1,
                                    residual_flag=False, training_flag=training_flag,
                                    activation_func1=tf.nn.leaky_relu, activation_func2=tf.nn.leaky_relu)

            layer = NL.ResNet_layer(layer, filter_num, 1, 1, 3, 1,
                                    residual_flag=False, training_flag=training_flag,
                                    activation_func1=tf.nn.leaky_relu, activation_func2=tf.nn.leaky_relu)

            layer = NL.conv_layer(layer, filter_num, 1, 1, training_flag=training_flag,
                                  activation_func=tf.nn.leaky_relu)

            route1 = layer

            layer = NL.conv_layer(layer, 2 * filter_num, 3, 1, training_flag=training_flag,
                                  activation_func=tf.nn.leaky_relu)
            return route1, layer

    def detect(self, input, anchors, img_size, training_flag=True):
        with tf.name_scope("DetectionLayer"):
            n_anchors = len(anchors)

            if self.transfer_learning_flag:
                layer = NL.conv_layer(input, n_anchors * (5 + self.n_classes), 1, 1,
                                      dropout_rate = 0.5, padding_type="SAME", input_dim="2d",
                                      training_flag=training_flag, use_bais_flag=True, batch_norm_flag=False,
                                      var_scope="new_conv_layer", layer_name="new_conv_layer", collection_name="new_conv_layer")
            else:
                layer = NL.conv_layer(input, n_anchors * (5 + self.n_classes), 1, 1,
                                      padding_type="SAME", input_dim="2d",
                                      training_flag=training_flag, use_bais_flag=True, batch_norm_flag=False)
            #print(layer)


            shape = layer.get_shape().as_list()
            grid_shape = shape[1:3]

            inputs = tf.reshape(layer, [-1, n_anchors * grid_shape[0] * grid_shape[1],
                                        5 + self.n_classes])

            strides = (img_size[0] // grid_shape[0], img_size[1] // grid_shape[1])

            box_centers, box_shapes, confidence, classes = \
                tf.split(inputs, [2, 2, 1, self.n_classes], axis=-1)

            x = tf.range(grid_shape[0], dtype=tf.float32)
            y = tf.range(grid_shape[1], dtype=tf.float32)
            x_offset, y_offset = tf.meshgrid(x, y)
            x_offset = tf.reshape(x_offset, (-1, 1))
            y_offset = tf.reshape(y_offset, (-1, 1))
            x_y_offset = tf.concat([x_offset, y_offset], axis=-1)
            x_y_offset = tf.tile(x_y_offset, [1, n_anchors])
            x_y_offset = tf.reshape(x_y_offset, [1, -1, 2])
            box_centers = tf.nn.sigmoid(box_centers)
            box_centers = (box_centers + x_y_offset) * strides

            anchors = tf.tile(anchors, [grid_shape[0] * grid_shape[1], 1])
            box_shapes = tf.exp(box_shapes) * tf.to_float(anchors)

            confidence = tf.nn.sigmoid(confidence)

            classes = tf.nn.sigmoid(classes)

            layer = tf.concat([box_centers, box_shapes,
                               confidence, classes], axis=-1)

            return layer


    def model_input(self):
        #p = self.para_list
        out_height = 416 // 32
        out_width = 416 // 32
        out_channels = 3 * (5 + self.n_classes)

        with tf.name_scope("input"):
            x = tf.placeholder(shape=(None, 416, 416, 3),
                               dtype=tf.float32,
                               name="x")
            tf.add_to_collection("input", x)

        with tf.name_scope("output"):
            y1 = tf.placeholder(shape=(None, out_height, out_width, out_channels), #169
                                dtype=tf.float32,
                                name="label_1")
            y2 = tf.placeholder(shape=(None, 2 * out_height, 2 * out_width, out_channels), #676
                                dtype=tf.float32,
                                name="label_2")
            y3 = tf.placeholder(shape=(None, 4 * out_height, 4 * out_width, out_channels), #1521
                                dtype=tf.float32,
                                name="label_3")
            tf.add_to_collection("y1", y1)
            tf.add_to_collection("y2", y2)
            tf.add_to_collection("y3", y2)
        self.x, self.y1, self.y2, self.y3= x, y1, y2, y3

    def model(self, training_flag = True):
        with tf.variable_scope(self.model_name):
            # Detection Layer 1
            route1, route2, layer = self.darknet53(self.x / 255)
            with tf.name_scope("DetectionBlock1"):
                route, layer = self.conv_block(layer, 512)
                detect1 = self.detect(layer,
                                      training_flag=training_flag,
                                      anchors=self.anchors[6:9],
                                      img_size=self.model_size)

            with tf.name_scope("DetectionBlock2"):
                # Detection Layer 2
                layer = NL.conv_layer(route, 256, 1, 1, training_flag=training_flag,
                                      activation_func=tf.nn.leaky_relu)
                upsample_size = route2.get_shape().as_list()
                layer = NL.upsample(layer, upsample_size, (1, 1, 1, 1), layer_name=None, collection_name=None)
                layer = tf.concat([layer, route2], axis=3)
                route, layer = self.conv_block(layer, 256)
                detect2 = self.detect(layer,
                                      training_flag=training_flag,
                                      anchors=self.anchors[3:6],
                                      img_size=self.model_size)

            with tf.name_scope("DetectionBlock3"):
                # Detection Layer 3
                layer = NL.conv_layer(route, 128, 1, 1, training_flag=training_flag,
                                      activation_func=tf.nn.leaky_relu)
                upsample_size = route1.get_shape().as_list()
                layer = NL.upsample(layer, upsample_size, (1, 1, 1, 1), layer_name=None, collection_name=None)
                layer = tf.concat([layer, route1], axis=3)
                route, layer = self.conv_block(layer, 128, training_flag=training_flag)
                detect3 = self.detect(layer,
                                      training_flag=training_flag,
                                      anchors=self.anchors[0:3],
                                      img_size=self.model_size)
            #print(detect1, detect2, detect3)

            ypred = tf.concat([detect1, detect2, detect3], axis=1)
            #print(ypred)

            return detect1, detect2, detect3, ypred











