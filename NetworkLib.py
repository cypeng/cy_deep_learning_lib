# -*- coding: utf-8 -*-
"""
@author: CY Peng

DataVer  - Author - Note
2019/06/23   CY     DL Fundemental Lib Release
2019/08/13   C.Y.   TensorFlow Module Solution
"""

import tensorflow as tf
import numpy as np

_conv_layer_count = 0
_conv_layer_unit_count = 0
_upsamle_count = 0
_pooling_layer_unit = 0
_dense_layer_count = 0
_dense_layer_unit = 0

def get_multi_training_variable_list(var_list=None, name_scope=None):
    if var_list is None:
        return get_training_variable_list()
    tran_var_list = []
    for var in var_list:
        tran_var_list.extend(get_training_variable_list(name_scope=name_scope, var_scope=var))
    return tran_var_list

def get_training_variable_list(name_scope=None, var_scope=None):
    if name_scope is None:
        if var_scope is None:
            return tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)
        return tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=var_scope)
    else:
        with tf.name_scope(name_scope):
            if var_scope is None:
                return tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)
            return tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=var_scope)

"""
Layer Variables Initialization 
"""
def wb_var_init(weight_shape, bias_shape, weights_setting=None):
    if weights_setting is None:
        weights_setting = [float(0.0), float(0.1)]

    weights = tf.get_variable("weights",
                              shape=weight_shape,
                              dtype=tf.float32,
                              initializer=tf.random_normal_initializer(mean=weights_setting[0],
                                                                       stddev=weights_setting[1]))
    biases = tf.get_variable("biases",
                             shape=bias_shape,
                             dtype=tf.float32,
                             initializer=tf.constant_initializer(0.0))
    return weights, biases

def gb_var_init(gamma_shape, beta_shape):
    gamma = tf.get_variable("gamma",
                            shape= gamma_shape,  # tf.ones([output_size])
                            dtype= tf.float32,
                            initializer=tf.ones_initializer())
    beta = tf.get_variable("beta",
                           shape= beta_shape,  # tf.ones([output_size])
                           dtype= tf.float32,
                           initializer=tf.ones_initializer())

    return gamma, beta

"""
Layer Variables Initialization
"""
def layer_var_init(weight_shape, bias_shape, weights_setting=None, name_scope=None, var_scope=None,
                   collection_name=None):
    if name_scope is None:
        if var_scope is None:
            weights, biases = wb_var_init(weight_shape, bias_shape, weights_setting)
        else:
            with tf.variable_scope(var_scope):
                weights, biases = wb_var_init(weight_shape, bias_shape, weights_setting)
    else:
        with tf.name_scope(name_scope):
            if var_scope is None:
                weights, biases = wb_var_init(weight_shape, bias_shape, weights_setting)
            else:
                with tf.variable_scope(var_scope):
                    weights, biases = wb_var_init(weight_shape, bias_shape, weights_setting)

    if collection_name is not None:
        tf.add_to_collection(collection_name+"_weights", weights)
        tf.add_to_collection(collection_name+"_bias", biases)

    return weights, biases


def batchnorm_var_init(gamma_shape, beta_shape, name_scope=None, var_scope=None,
                       collection_name=None):

    if name_scope is None:
        if var_scope is None:
            gamma, beta = gb_var_init(gamma_shape, beta_shape)
        else:
            with tf.variable_scope(var_scope):
                gamma, beta = gb_var_init(gamma_shape, beta_shape)
    else:
        with tf.name_scope(name_scope):
            if var_scope is None:
                gamma, beta = gb_var_init(gamma_shape, beta_shape)
            else:
                with tf.variable_scope(var_scope):
                    gamma, beta = gb_var_init(gamma_shape, beta_shape)

    if collection_name is not None:
        tf.add_to_collection(collection_name+"_gamma", gamma)
        tf.add_to_collection(collection_name+"_beta", beta)

    return gamma, beta

def yolo_detect(input, anchor, n_classes=20, layer_name=None,
                collection_name=None):  # in tensor has shape (batch_size, height, width, 255)
    if layer_name is None:
        layer_name = "yolo_detect"

    n_anchors = len(anchor)
    batch_size, height, width, in_channels = input.get_shape().as_list()
    # spliting to three box for comparing to the three anchors
    # print(input)
    recover_input = tf.reshape(input, [-1, n_anchors * height * width, (5 + n_classes)])
    # print(recover_input)
    obj, box_center, box_length, i_class = tf.split(recover_input, [1, 2, 2, n_classes], axis=-1)
    # print(obj, box_center, box_length, i_class)

    strides = (416 // width, 416 // height)

    confidence = tf.nn.sigmoid(obj)

    x = tf.range(width, dtype=tf.float32)
    y = tf.range(height, dtype=tf.float32)
    x_offset, y_offset = tf.meshgrid(x, y)
    x_offset = tf.reshape(x_offset, (-1, 1))
    y_offset = tf.reshape(y_offset, (-1, 1))
    x_y_offset = tf.concat([x_offset, y_offset], axis=-1)
    x_y_offset = tf.tile(x_y_offset, [1, n_anchors])
    x_y_offset = tf.reshape(x_y_offset, [1, -1, 2])
    box_centers = tf.nn.sigmoid(box_center)
    box_centers = (box_centers + x_y_offset) * strides

    anchors = tf.tile(anchor, [width * height, 1])
    box_shapes = tf.exp(box_length) * tf.to_float(anchors)

    classes = tf.nn.sigmoid(i_class)

    with tf.name_scope(layer_name):
        layer = tf.concat([confidence, box_centers, box_shapes,
                           classes], axis=-1, name="_yolo_dect")
        if collection_name is not None:
            tf.add_to_collection(collection_name + "_yolo_dect", layer)
    # print(layer)
    return layer

"""
ResNet

"""

def ResNet_layer(input, filter_num1, kernel_size1, strides1, kernel_size2, strides2,
                 filter_num2=None, residual_flag=True, training_flag=True,
                 use_bais_flag=False, batch_norm_flag=True,
                 batch_norm_decay1=0.9, batch_norm_epsilon1=1e-05, dropout_rate1=0,
                 activation_func1=None, collection_name1=None,
                 batch_norm_decay2=0.9, batch_norm_epsilon2=1e-05, dropout_rate2=0,
                 activation_func2=None, collection_name2=None):

    if filter_num2 is None:
        filter_num2 = 2 * filter_num1

    with tf.name_scope("ResNet"):
        shortcut = input

        conv = conv_layer(input, filter_num1, kernel_size1, strides1,
                          batch_norm_decay=batch_norm_decay1, batch_norm_epsilon=batch_norm_epsilon1,
                          dropout_rate=dropout_rate1, padding_type="SAME", input_dim="2d",
                          training_flag=training_flag, use_bais_flag=use_bais_flag, batch_norm_flag=batch_norm_flag,
                          activation_func=activation_func1, collection_name=collection_name1)

        conv = conv_layer(conv, filter_num2, kernel_size2, strides2,
                          batch_norm_decay=batch_norm_decay2, batch_norm_epsilon=batch_norm_epsilon2,
                          dropout_rate=dropout_rate2, padding_type="SAME", input_dim="2d",
                          training_flag=training_flag, use_bais_flag=use_bais_flag, batch_norm_flag=batch_norm_flag,
                          activation_func=activation_func2, collection_name=collection_name2)

        if residual_flag:
            layer_output = conv + shortcut
        else:
            layer_output = conv

        return layer_output

def ResNet_unit(input, weight_shape1, bias_shape1, strides_shape1, padding_type1,
                weight_shape2, bias_shape2, strides_shape2, padding_type2,
                input_dim1="2d", input_dim2="2d",
                activation_func=None, activation_para=None, var_scope=None, layer_name=None, collection_name=None):
    var_scope1, layer_name1, collection_name1 = var_scope, layer_name, collection_name
    var_scope2, layer_name2, collection_name2 = var_scope, layer_name, collection_name

    if var_scope is not None:
        var_scope1 = var_scope + "_hid_lay1"
        var_scope2 = var_scope + "_hid_lay2"

    if collection_name is not None:
        collection_name1 = collection_name + "_hid_lay1"
        collection_name2 = collection_name + "_hid_lay2"

    if layer_name is not None:
        layer_name1 = layer_name + "_hid_lay1"
        layer_name2 = layer_name + "_hid_lay2"

    shortcut = input

    conv = conv_layer_uint(input, weight_shape1, bias_shape1, strides_shape1, padding_type1, input_dim=input_dim1,
                           weights_setting=None, batch_norm_flag=bool(1), activation_func=activation_func,
                           activation_para=activation_para, var_scope=var_scope1, layer_name=layer_name1,
                           collection_name=collection_name1)

    conv = conv_layer_uint(conv, weight_shape2, bias_shape2, strides_shape2, padding_type2, input_dim=input_dim2,
                           weights_setting=None, batch_norm_flag=bool(1), activation_func=activation_func,
                           activation_para=activation_para, var_scope=var_scope2, layer_name=layer_name2,
                           collection_name=collection_name2)

    layer_output = conv
    res_output = conv + shortcut

    return layer_output, res_output

"""
Ref.
tf.layers.batch_normalization

"""

def mean_var_with_update(ema, pop_mean, pop_variance):
    ema_apply_op = ema.apply([pop_mean, pop_variance])
    with tf.control_dependencies([ema_apply_op]):
        return tf.identity(pop_mean), tf.identity(pop_variance)

def batch_normalization(input, gamma, beta, decay=0.99, epsilon=1e-3):

    batch_mean, batch_variance = tf.nn.moments(input, [1, 2, 3])

    ema = tf.train.ExponentialMovingAverage(decay=decay)
    train_mean, train_variance = mean_var_with_update(ema, batch_mean, batch_variance)
    batch_norm = tf.nn.batch_normalization(input, train_mean, train_variance, beta, gamma, epsilon)

    return batch_norm

"""
Ref.
https://medium.com/@chih.sheng.huang821/%E6%B7%B1%E5%BA%A6%E5%AD%B8%E7%BF%92-%E7%89%A9%E4%BB%B6%E5%81%B5%E6%B8%ACyolov1-yolov2%E5%92%8Cyolov3-cfg-%E6%AA%94%E8%A7%A3%E8%AE%80-%E4%BA%8C-f5c2347bea68
"""

def upsample(input, out_shape, strides_shape, layer_name=None, collection_name=None):
    global _upsamle_count
    if layer_name is None:
        layer_name = "upsample"+str(_upsamle_count)
        _upsamle_count += 1

    with tf.name_scope(layer_name):
        new_height, new_width = out_shape[1], out_shape[2]
        strides_height, strides_width = strides_shape[1], strides_shape[2]
        upsample = tf.image.resize_nearest_neighbor(input, (new_height * strides_height, new_width * strides_width),
                                                    name='upsampled_op')

        if collection_name is not None:
            tf.add_to_collection(collection_name + "_upsample", upsample)

    # unsample = tf.nn.conv2d_transpose(in_tensor, kernel, output_shape, strides, name="out")
    return upsample

"""
Ref.
tf.keras.layers.Conv1D, tf.keras.layers.Conv2D, tf.keras.layers.Conv3D
tf.keras.layers.Conv2DTranspose, tf.keras.layers.Conv3DTranspose

1d input: A 3D Tensor. Must be of type float16, float32, or float64.
2d input:
Given an input tensor of shape [batch, in_height, in_width, in_channels] and 
a filter / kernel tensor of shape [filter_height, filter_width, in_channels, out_channels],
3d input:
A Tensor. Must be one of the following types: half, bfloat16, float32, float64. 
Shape [batch, in_depth, in_height, in_width, in_channels].
N-D input:
An (N+2)-D Tensor of type T, of shape [batch_size] + input_spatial_shape + [in_channels] 
if data_format does not start with "NC" (default), or [batch_size, in_channels] + 
input_spatial_shape if data_format starts with "NC".
"""

def conv_layer(input, filter_num, kernel_size, strides,
               batch_norm_decay=0.9, batch_norm_epsilon=1e-05, dropout_rate=0,
               activation_para = 0.1, padding_type="SAME", input_dim="2d",
               training_flag=True, use_bais_flag=False, batch_norm_flag=True,
               activation_func=None, var_scope=None, layer_name=None, collection_name=None):
    global _conv_layer_count
    weights_setting = [float(0.0), float(0.01)]

    if layer_name is None:
        layer_name = "conv_layer_"+str(_conv_layer_count)
    else:
        layer_name = layer_name + str(_conv_layer_count)
    if var_scope is None:
        var_scope = "conv_layer_"+str(_conv_layer_count)
    else:
        var_scope = var_scope + str(_conv_layer_count)
    _conv_layer_count += 1

    with tf.name_scope(layer_name):
        with tf.variable_scope(var_scope):
            if input_dim is "1d":
                layer = tf.layers.conv1d(inputs=input, filters=filter_num, kernel_size=kernel_size,
                                         strides=strides, padding=padding_type, use_bias=use_bais_flag,
                                         trainable=training_flag,
                                         kernel_initializer=tf.random_normal_initializer(mean=weights_setting[0],
                                                                                         stddev=weights_setting[1]),
                                         bias_initializer=tf.constant_initializer(0.0))
            elif input_dim is "3d":
                layer = tf.layers.conv3d(inputs=input, filters=filter_num, kernel_size=kernel_size,
                                         strides=strides, padding=padding_type, use_bias=use_bais_flag,
                                         trainable=training_flag,
                                         kernel_initializer=tf.random_normal_initializer(mean=weights_setting[0],
                                                                                         stddev=weights_setting[1]),
                                         bias_initializer= tf.constant_initializer(0.0))
            else:
                layer = tf.layers.conv2d(inputs=input, filters=filter_num, kernel_size=kernel_size,
                                         strides=strides, padding=padding_type, use_bias=use_bais_flag,
                                         trainable=training_flag,
                                         kernel_initializer=tf.random_normal_initializer(mean=weights_setting[0],
                                                                                         stddev=weights_setting[1]),
                                         bias_initializer= tf.constant_initializer(0.0))

            if batch_norm_flag:
                layer = tf.layers.batch_normalization(inputs=layer, axis=3, momentum=batch_norm_decay,
                                                      epsilon=batch_norm_epsilon,
                                                      scale=True, training=training_flag)

            if dropout_rate > 0:
                layer = tf.layers.dropout(inputs=layer, rate=dropout_rate, training=training_flag)

            if activation_func:
                if activation_para:
                    layer = activation_func(layer, name="output", alpha=activation_para)
                else:
                    layer = activation_func(layer, name="output")

            if collection_name:
                tf.add_to_collection(collection_name, layer)

            return layer

def conv_layer_uint(input, weight_shape, bias_shape, strides_shape, padding_type, input_dim="2d", keep_prob=1,
                    gamma_shape=None, beta_shape=None, batch_norm_flag=bool(0), weights_setting=None,
                    activation_func=None, activation_para=None, var_scope=None, layer_name=None,
                    collection_name=None):
    global _conv_layer_unit_count

    if layer_name is None:
        layer_name = "conv_layer_"+str(_conv_layer_unit_count)
        _conv_layer_unit_count += 1

    with tf.name_scope(layer_name):
        if batch_norm_flag is bool(1):
            gamma, beta = batchnorm_var_init(gamma_shape, beta_shape, var_scope=var_scope,
                                             collection_name=collection_name)
            weights, _ = layer_var_init(weight_shape, bias_shape, weights_setting=weights_setting,
                                        var_scope=var_scope, collection_name=collection_name)
        else:
            weights, biases = layer_var_init(weight_shape, bias_shape, weights_setting=weights_setting,
                                             var_scope=var_scope, collection_name=collection_name)

        if input_dim == "1d":
            conv = tf.nn.conv1d(input, weights, strides=strides_shape, padding=padding_type, name="conv1d_op")
        elif input_dim == "2d":
            conv = tf.nn.conv2d(input, weights, strides=strides_shape, padding=padding_type, name="conv2d_op")
        elif input_dim == "3d":
            conv = tf.nn.conv3d(input, weights, strides=strides_shape, padding=padding_type, name="conv3d_op")
        else:
            conv = tf.nn.convolution(input, weights, strides=strides_shape, padding=padding_type, name="conv_op")

        if collection_name:
            tf.add_to_collection(collection_name + "_conv", conv)

        if batch_norm_flag:
            layer = batch_normalization(conv, gamma, beta, decay=0.99, epsilon=1e-3)
            if collection_name is not None:
                tf.add_to_collection(collection_name + "_batch_norm", layer)
        else:
            layer = tf.nn.bias_add(conv, biases, name="output")
            if collection_name is not None:
                tf.add_to_collection(collection_name + "_bias_add", layer)

        layer = tf.nn.dropout(layer, keep_prob, name="drop")

        if activation_func is not None:
            if activation_para is None:
                layer = activation_func(layer, name="output")
            else:
                layer = activation_func(layer, alpha=activation_para, name="output")

            if collection_name:
                tf.add_to_collection(collection_name + "_activation", layer)

        # print(layer)
        return layer

""""
Ref.
tf.keras.layers:
AveragePooling1D, AveragePooling2D, AveragePooling3D
GlobalAveragePooling1D, GlobalAveragePooling2D, GlobalAveragePooling3D
MaxPool1D, MaxPool2D, MaxPool3D
GlobalMaxPool1D, GlobalMaxPool2D, GlobalMaxPool3D

2d input:                           
inputs: A 4-D Tensor of shape [batch, height, width, channels] and 
type float32, float64, qint8, quint8, or qint32.                                  

3d input:
inputs: Must be one of the following types: half, bfloat16, float32, float64. Shape 
[batch, depth, rows, cols, channels] tensor to pool over

nd input:
Tensor of rank N+2, of shape [batch_size] + input_spatial_shape + [num_channels] 
if data_format does not start with "NC" (default), or [batch_size, num_channels] + input_spatial_shape 
if data_format starts with "NC". Pooling happens over the spatial dimensions only.
"""

def pooling_layer_uint(input, kernel_shape, strides_shape, padding_type, input_dim="2d", pooling_type="MAX",
                       layer_name=None, collection_name=None):
    global _pooling_layer_unit
    if layer_name is None:
        layer_name = "pooling_layer"+str(_pooling_layer_unit)
        _pooling_layer_unit += 1

    with tf.name_scope(layer_name):
        if input_dim == "2d":
            if pooling_type == "MAX":
                layer = tf.nn.max_pool(input,
                                       ksize=kernel_shape,
                                       strides=strides_shape,
                                       padding=padding_type,
                                       name="max_pool2d_op")
            if pooling_type == "AVG":
                layer = tf.nn.avg_pool(input,
                                       ksize=kernel_shape,
                                       strides=strides_shape,
                                       padding=padding_type,
                                       name="avg_pool2d_op")
        elif input_dim == "3d":
            if pooling_type == "MAX":
                layer = tf.nn.max_pool3d(input,
                                         ksize=kernel_shape,
                                         strides=strides_shape,
                                         padding=padding_type,
                                         name="max_pool3d_op")
            if pooling_type == "AVG":
                layer = tf.nn.avg_pool3d(input,
                                         ksize=kernel_shape,
                                         strides=strides_shape,
                                         padding=padding_type,
                                         name="avg_pool3d_op")
        else:
            layer = tf.nn.pool(input, kernel_shape, pooling_type, padding_type, strides=strides_shape,
                               name="pool_op")
        if collection_name is not None:
            tf.add_to_collection(collection_name + "_pool", layer)
        #print(layer)
        return layer

"""
Ref.
tf.keras.layers.Dense

input, filter_num, kernel_size, strides,
               batch_norm_decay=0.9, batch_norm_epsilon=1e-05, dropout_rate=0,
               activation_para = 0.1, padding_type="SAME", input_dim="2d",
               training_flag=True, use_bais_flag=False, batch_norm_flag=True,
               activation_func=None, var_scope=None, layer_name=None, collection_name=None
"""

def dense_layer(input, output_size, dropout_rate = 0, activation_para = None,
                training_flag = True, kernel_regularizer=None, activity_regularizer=None,
                use_bais_flag = False, bias_regularizer= None,
                activation_func = None, var_scope = None, layer_name = None, collection_name=None):
    global _dense_layer_count

    if layer_name is None:
        layer_name = "dense_layer_" + str(_dense_layer_count)
    else:
        layer_name = layer_name + str(_dense_layer_count)

    if var_scope is None:
        var_scope = "dense_layer_" + str(_dense_layer_count)
    else:
        var_scope = var_scope + str(_dense_layer_count)

    _dense_layer_count += 1

    with tf.name_scope(layer_name):
        with tf.variable_scope(var_scope):
            layer = tf.layers.dense(
                input,
                output_size,
                activation=None,
                use_bias=use_bais_flag,
                bias_initializer=tf.zeros_initializer(),
                kernel_regularizer=kernel_regularizer,
                bias_regularizer=bias_regularizer,
                activity_regularizer=activity_regularizer,
                trainable=training_flag,
                name="layer_out"
            )

            if dropout_rate > 0:
                layer = tf.layers.dropout(inputs=layer, rate=dropout_rate, training=training_flag, name="dropout")

            if activation_func:
                if activation_para:
                    layer = activation_func(layer, name="output", alpha=activation_para)
                else:
                    layer = activation_func(layer, name="output")

            if collection_name:
                tf.add_to_collection(collection_name, layer)

            return layer

"""
Ref:
tf.keras.layers.Dense
tf.keras.layers.Softmax:
dense for keras.activations.softmax
"""

def dense_layer_uint(input, weight_shape, bias_shape, weights_setting=None, activation_func=None, var_scope=None,
                     layer_name=None, collection_name=None):
    global _dense_layer_unit
    if layer_name is None:
        layer_name = "dense_layer" + str(_dense_layer_unit)
        _dense_layer_unit += 1

    with tf.name_scope(layer_name):
        weights, biases = layer_var_init(weight_shape, bias_shape, weights_setting=weights_setting,
                                         var_scope=var_scope, collection_name=collection_name)
        matul = tf.matmul(input, weights, name="dense_op")

        if collection_name is not None:
            tf.add_to_collection(collection_name + "_matual", matul)

        if activation_func is None:
            layer = tf.nn.bias_add(matul, biases, name="output")
            if collection_name is not None:
                tf.add_to_collection(collection_name + "_bias_add", layer)
            return layer

        layer = activation_func(tf.nn.bias_add(matul, biases), name="output")
        if collection_name is not None:
            tf.add_to_collection(collection_name + "act_bias_add", layer)
        return layer

"""
Ref.
1. tf.layers.Flatten: Fixed 0 axis
2. tf.layers.flatten = from tensorflow.contrib.layers import flatten
"""

def flat_layer(input, axis0_fixed=None):
    if axis0_fixed is None:
        return tf.layers.flatten(input)
    return tf.layers.Flatten(input)
